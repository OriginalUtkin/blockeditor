Soubor : README.txt
Vytvoril : Utkin Kirill (xutkin00)
K projektu: ICP, Alikace BlockEditor 2018

Tento projekt implementuje editator blokovych schemat. Aplikace umoznuje vytvaret, editovat, ukladat a nacitat blokova schema. 
Kazda shcema je slozena z bloku, portu a propojeni mezi nimi. Pro ulozeni vstupnich dat je nutne vytvorit input block ; pro pristup k vysledkum
je nutne pouzit output block. Program zajistuje, zda-li vsichni vstupni porty jsou napojeny na vystupni a input block obsahuje nastavenou
hodnotu odlisnou od NaN. V pripade nalezeni smycky system vypocet ukonci.


Nedostatky:
Bloky podporuiji pouze jeden typ dat (aritmeticka)
