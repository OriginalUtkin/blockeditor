#
# ICP 2018 
# 	
# autor: Utkin Kirill(xutkin00)
# date: 6. May 2018
ifneq ("$(wildcard /usr/bin/qmake-qt5)","")
QMAKE = "qmake-qt5"
else
QMAKE = "qmake"
endif
ifneq ("$(wildcard /usr/local/share/Qt-5.2.1/5.2.1/gcc_64/bin/qmake)","")
QMAKE = "/usr/local/share/Qt-5.2.1/5.2.1/gcc_64/bin/qmake"
endif

all:
	cd src && $(QMAKE) -o Makefile && make
	mv src/BlockEditor ./editor2018

run:
	all


# creating .zip
pack: clean
	zip -r xutkin00.zip Makefile src/* doc/* examples/* README

#doxy
doxygen:
	mkdir doc
	doxygen src/doxy.conf


#clean all
clean:
	rm -rf doc
	rm -rf src/qrc_*
	rm -rf src/moc_*
	rm -rf src/*.o
	rm -rf src/Makefile
	rm -rf doc/*
