#include <QtWidgets>
#include <QApplication>
#include "menuview.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MenuView window;
    window.show();

    return app.exec();
}
