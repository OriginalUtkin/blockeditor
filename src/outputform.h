/**
  @file OutputForm.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro OutputForm.cpp
*/

#ifndef OUTPUTFORM_H
#define OUTPUTFORM_H

#include <QWidget>

namespace Ui {
class OutputForm;
}

class OutputForm : public QWidget
{
    Q_OBJECT

public:
    explicit OutputForm(const double resultValue, QWidget *parent = nullptr);
    ~OutputForm();

private:
    Ui::OutputForm *ui;
};

#endif // OUTPUTFORM_H
