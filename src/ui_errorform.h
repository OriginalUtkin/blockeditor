/********************************************************************************
** Form generated from reading UI file 'errorform.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERRORFORM_H
#define UI_ERRORFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ErrorForm
{
public:
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *ErrorForm)
    {
        if (ErrorForm->objectName().isEmpty())
            ErrorForm->setObjectName(QStringLiteral("ErrorForm"));
        ErrorForm->resize(240, 158);
        label = new QLabel(ErrorForm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(80, 10, 101, 51));
        label_2 = new QLabel(ErrorForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 50, 221, 101));
        label_2->setLineWidth(5);
        label_2->setWordWrap(true);

        retranslateUi(ErrorForm);

        QMetaObject::connectSlotsByName(ErrorForm);
    } // setupUi

    void retranslateUi(QWidget *ErrorForm)
    {
        ErrorForm->setWindowTitle(QApplication::translate("ErrorForm", "Form", 0));
        label->setText(QApplication::translate("ErrorForm", "       Error", 0));
        label_2->setText(QApplication::translate("ErrorForm", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class ErrorForm: public Ui_ErrorForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERRORFORM_H
