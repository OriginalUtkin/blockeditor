/**
  @file BlockData.h
  @author Utkin Kirill, xutkin00
  @brief popisuje data v load souboru
*/

#ifndef BLOCKDATA_H
#define BLOCKDATA_H

#include <string>

typedef struct{

    std::string objectType; // typ nactene data z load souboru
    std::string blockName; // jmeno bloku

    bool blockState; //stav bloku
    int blockType; // druh bloku

    unsigned int inputPortNum; //pocet vstupnich portu bloku
    unsigned int outputPortNum; //pocet vystupnich portu bloku

    std::vector<double> inputData; //data v inputPorts
    double dataForBlock; // vnitrni stav bloku
    double outputData; // vysledek operace bloku

    double xScenePosition; // pozice bloku / output portu propojeni(x)
    double yScenePosition; // pozice bloku / output portu  propojeni (y)

    double xIScenePosition; //pozice input portu (x). Nastavuje pouze u propojeni
    double yIScenePosition; //pozice input portu (y). Nastavuje pouze u propojeni

}blockData;

#endif // BLOCKDATA_H
