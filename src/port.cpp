/**
  @file Port.cpp
  @author Utkin Kirill, xutkin00
  @brief Soubor obsahuje tridu Port, ktera reprezentuje logiku portu uvnitr BlockView
  @see BlockView.h
*/

#include "port.h"

// Textova reprezentace enumu PortType
const std::string Port::EnumPortType[] = {"Input", "Output"};

// Textova reprezentace enumu DataType
const std::string Port::EnumDataType[] = {"Arithmetic", "Logic"};


/**
 * @brief Port::Port implicitni konstruktor tridy. Po vytvoreni objektu nastavuje pole data na hodnotu NaN
 */
Port::Port(){

    this->data = std::numeric_limits<double>::quiet_NaN();
}


/**
 * @brief Port::setParent getter metoda ; nastavuje pole parent
 * @param parentBlock ukazatel na Block
 * @see Block.h
 */
void Port::setParent(const Block *parentBlock){

    this->parent = parentBlock;
}


/**
 * @brief Port::setDataType getter metoda; nastavuje pole dataType na hodnotu type
 * @param type DataType hodnota, reprezentujici data v portu
 * @see DataType
 */
void Port::setDataType(DataType type) {

    this->dataType = type;
}


/**
 * @brief Port::getDataType getter metoda ; vrati typ dat, kteri jsou ulozene v portu
 * @return hodnotu pole dataType
 * @see PortData.h
 */
DataType Port::getDataType() const{

    return this->dataType;
}


/**
 * @brief Port::setPortType setter metoda ; nastavi typ portu na hodnotu type
 * @param type typ portu
 * @see PortType
 */
void Port::setPortType(PortType type){

    this->portType = type;
}


/**
 * @brief Port::setConnected setter metoda; nastavi pole connected na hodnotu value
 * @param value hodnota typu bool. Nastavuje na true, pokud port je pripojen, jinak false
 */
void Port::setConnected(bool value){

    this->connected = value;
}


/**
 * @brief Port::getPortType getter metoda ;  Vrati typ portu
 * @return typ portu
 * @see PortType
 */
PortType Port::getPortType() const{

    return this->portType;
}


/**
 * @brief Port::setDatanasavuje Data pro predani do bloku. Bude pak pouzita pro provedeni operaci uvnitr bloku
 * @param value data
 */
void Port::setData(double value){

    this->data = value;
}


/**
 * @brief Port::getData getter metoda ; vrati double hodnotu z pole Data prislusneho portu
 * @return double hodnota
 */
double Port::getData() const{

    return this->data;
}


/**
 * @brief Port::getPortTypeString prevadi hodnotu EnumPortValue do String
 * @param enumValue int hodnota reprezentujici hodnotu z enum seznamu
 * @return odpovidajici polozka z enum seznamu ve string formatu
 */
std::string Port::getPortTypeString(int enumValue) const{

    return this->EnumPortType[enumValue];
}


/**
 * @brief Port::getParent getter metoda, vrati odkaz na Blok, ktery tento port obdrzuje
 * @return Block* hodnotu
 */
const Block *Port::getParent() const{

    return this->parent;
}


/**
 * @brief Port::isConnected getter metoda, vrati hodnotu z pole connected prislusneho portu
 * @return true pokud port ve stavu "Pripojen", jinak vrati false
 */
bool Port::isConnected() const{

    return this->connected;
}


/**
 * @brief operator << pretizena operace pro vytvoreni textove reprezentaci portu
 * @param oshreference do OutputStream
 * @param port reference to port
 * @return reference to OutputStream
 */
std::ostream& operator << (std::ostream& os, const Port& port){

    os << port.parent << " "
       << port.getPortTypeString(port.portType) << " "
       << port.data << " "
       << port.connected << std::endl;

    return os;
}
