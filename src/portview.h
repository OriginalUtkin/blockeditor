/**
  @file PortView.h
  @author Utkin Kirill (xutkin00)
  @brief Hlavickovy soubor pro tridu PortView
  @see Port.cpp
*/


#ifndef PORTVIEW_H
#define PORTVIEW_H

#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include <QFontMetrics>
#include <QPen>

#include "connectionview.h"

class BlockView;
class ConnectionView;
class Port;

class PortView : public QGraphicsPathItem
{
public:

    // Definice typu objektu uvnitr scene (http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var)
    enum { Type = QGraphicsItem::UserType + 1 };


    /**
     * @brief PortView::PortView Primarni konstruktor tridy
     * @param parent
     */
    PortView(QGraphicsItem *parent = nullptr);


    /**
     * @brief PortView::~PortView destruktor tridy
     */
    ~PortView();

    Port* portBackend = nullptr; // logika portu

    // settery portu

    /**
     * @brief PortView::setParentView metoda nastavuje odkaz na blok, ke kteremu patri tento port
     * @param parentAdd ukazatel na rodic. blok
     */
    void setParentView(BlockView* parentAdd);

    //gettery portu

    /**
     * @brief PortView::getPortWidth getter metoda ; vrati vysku (=sirce) portu
     * @return hodnotu int , reprezentujici velikost portu
     * @see Block.cpp
     */
    int getPortWidth() const;


    /**
     * @brief PortView::type vrati typ objektu v ramci sceny
     * @return type objektu
     * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
     */
    int type() const;
    bool getPortType() const ;
    bool isConnected(PortView*);
    QVector<ConnectionView *> &getConnectionList();
    BlockView* getParent() const;

    QVector<ConnectionView*> connList;


protected:

    /**
     * @brief PortView::itemChange meni propojeni mezi bloky pri zmene pozice bloku
     * @param change typ zmeny
     * @param value
     * @return QGraphicsItem::itemChange
     * @see http://doc.qt.io/qt-5/qgraphicsitem.html#itemChange
     */
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    BlockView *parentBlockView = nullptr; // View bloku, ktery tento port obashuje
    int recWidth = 0 ;
    int portMargin = 0;
};

#endif // PORTVIEW_H
