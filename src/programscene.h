/**
  @file ProgramScene.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro ProgramScene.cpp
  @see ProgramScene.cpp
*/

#ifndef PROGRAMSCENE_H
#define PROGRAMSCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QEvent>
#include <QGraphicsSceneMouseEvent>
#include <assert.h>
#include <fstream>
#include <iostream>

#include "block.h"
#include "inputform.h"
#include "outputform.h"
#include "parser.h"
#include "connectionview.h"
#include "blockview.h"
#include "model.h"
#include "portview.h"

class QGraphicsScene;
class ConnectionView;
class QGraphicsItem;
class QPointF;
class BlockView;
class Block;

class ProgramScene : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief ProgramScene::ProgramScene Primarni konstruktor
     * @param parent
     */
    explicit ProgramScene(QObject *parent = nullptr);


    /**
     * @brief ProgramScene::setScene metoda nastavuje event filter pro scene
     * @param s scene objekt, vytvoreny pri spusteni programu
     * @see http://doc.qt.io/qt-5/qobject.html#installEventFilter
     */
    void setScene(QGraphicsScene *scene);


    /**
     * @brief ProgramScene::eventFilter zpracuje eventy ze scene a dovoluje iterakci s objekty uvnitr scene
     * @param o
     * @param mouseEvent event od mysi
     * @return true v pripade zpracovani eventu
     * @see http://doc.qt.io/qt-5/eventsandfilters.html
     */
    bool eventFilter(QObject *object, QEvent *mouseEvent);


    /**
     * @brief ProgramScene::save ulozeni obsahu sceny do souboru
     * @param ds stream pro ulozeni sceny do souboru
     */
    void save(std::ofstream& ds);


    /**
     * @brief ProgramScene::reloadScene metoda vycisti scenu pred nahranim souboru
     * @todo je nutne smazat objekty
     */
    void reloadScene();

private:

    /**
     * @brief ProgramScene::itemAt metoda zajistuje jake objekty nachazeji na pozici pos
     * @param pos pozice ; coord. ziskavaji po stisknuti tlacitka mysi
     * @return seznam objektu, kteri nachazeji na pozici pos
     */
    QGraphicsItem *itemAt(const QPointF&);

private:
    QGraphicsScene *scene;
    ConnectionView *potentialConnection = nullptr;
    QVector<inputForm*> myInputForm;
    QVector<OutputForm*> outputForms;
};

#endif // PROGRAMSCENE_H
