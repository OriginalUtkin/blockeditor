/**
  @file AdditionalBlockFactry
  @author Utkin Kirill, xutkin00
  @brief Defenice tovarny pro vytvoreni Backendu u Add block
  @see AdditionalBlcokFactory.h
*/

#include "additionalblockfactory.h"

AdditionalBlockFactory::AdditionalBlockFactory()
{

}

Block* AdditionalBlockFactory::createBackend(const unsigned int inputPortNum, const unsigned int outputPortNum){
    return new AdditionBlock(inputPortNum, outputPortNum);
}
