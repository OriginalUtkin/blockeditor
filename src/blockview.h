/**
  @file BlockView.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro BlockView.cpp
  @see BlockView.cpp
*/

#ifndef BLOCKVIEW_H
#define BLOCKVIEW_H

#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneHoverEvent>
#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include <QBrush>
#include <QPainter>
#include <QtWidgets>
#include <QtGui>
#include <QPen>
#include <iostream>
#include <fstream>

#include "portview.h"
#include "block.h"

class PortView;
class Block;
class Port;

class BlockView : public QGraphicsPathItem{

public:

    enum { Type = QGraphicsItem::UserType + 3 };  // Definice typu objektu uvnitr scene (http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var)

    Block* blockBackend = nullptr; // logika bloku


    /**
     * @brief BlockView::BlockView Primarni konsruktror ;provadi vykresleni bloku po vytvoreni
     * @param parent
     */
    BlockView(QGraphicsItem *parent = nullptr);


    /**
     * @brief BlockView::~BlockView destruktor bloku
     */
    virtual ~BlockView();


    /**
     * @brief BlockView::addPort vytvari View pro port uvnitr bloku
     * @param backend logika portu
     * @return ukazatel na view portu
     */
    PortView* addPort(Port *backend);


    /**
     * @brief BlockView::save metoda uklada Block vcetne vsech potr. inormaci do souboru
     * @param ds data stream pro ulozeni
     * @see textRepresentation() na konci souboru
     */
    void save(std::ofstream &);


    /**
     * @brief BlockView::paint prekresli block
     * @param painter ukazatel na objekt pro vykresleni
     * @param option nepouziva se
     * @param widget nepouziva se
     */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void changeColor();


    /**
     * @brief BlockView::ports getter metoda ; vrati Seznam vsech portu prislusneho bloku
     * @return List seznam portu
     */
    QVector<PortView*> getPortsList();


    /**
     * @brief BlockView::type vrati typ objektu v ramci sceny
     * @return type objektu
     * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
     */
    int type() const ;

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void hoverEnterEvent(QGraphicsSceneHoverEvent*);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent*);

private:

    /**
     * @brief textRepresentation metoda vrati textovou reprezentaci bloku
     * @return textovy retezec, reprezentujici stav bloku pro ulozeni
     */
    std::string textRepresentation();


    int blockWidth = 0; //pocatecni sirska bloku
    int blockHeight = 0; // vyska bloku

    int startWidth = 10; // pocatecni sirka bloku
    int startHeight = 5; //poacateci vyska bloku
};

#endif // BLOCKVIEW_H
