/**
  @file ConnectionView.cpp
  @author Utkin Kirill, xutkin00
  @brief Implementace View a Backendu pro propojeni mezi bloky
  @todo Nepodporuje ruzne typy dat (Doesn't accept other data types)
*/

#include "connectionview.h"


/**
 * @brief ConnectionView::ConnectionView Primarni konstruktor tridy, ktery vytvari vzhled propojeni
 * @param parent
 */
ConnectionView::ConnectionView(QGraphicsItem *parent) : QGraphicsPathItem(parent){

    setPen(QPen(Qt::black, 2));
    setBrush(Qt::NoBrush);
    setZValue(-1);
    setAcceptHoverEvents(true);
}

/**
 * @brief ConnectionView::~ConnectionView destruktor tridy. nastavuje pole input / output portu na false a odstrani zaznam z
 * propoje
 */
ConnectionView::~ConnectionView()
{
    if(outputPortAdd){
        outputPortAdd->portBackend->setConnected(false);
        outputPortAdd->connList.remove(outputPortAdd->connList.indexOf(this));
    }

    if(inputPortAdd){
        inputPortAdd->portBackend->setConnected(false);
        inputPortAdd->connList.remove(inputPortAdd->connList.indexOf(this));
    }
}

/**
 * @brief ConnectionView::setOutpuPortPos setter metoda ; nastavi pozici vystupniho portu.
 * @param tmpPosition hodnota typu QPointF, obsahujici pozici vystupniho portu
 */
void ConnectionView::setOutpuPortPos(const QPointF& tmpPosition){

    outputPortPos = tmpPosition;
}


/**
 * @brief ConnectionView::setInputPortPos setter metoda ; nastavi pozici vstupniho portu.
 * @param tmpPosition hodnota typu QPointF, obsahujici pozici vstupniho portu
 */
void ConnectionView::setInputPortPos(const QPointF& tmpPosition){

    inputPortPos = tmpPosition;
}

/**
 * @brief ConnectionView::setOutputPort setter metoda ; ulozi view vystupniho portu do pole outputPortAdd pristup do
 * logiky portu pri provadeni operaci.
 * @param portAddress adresa view vystupniho portu
 */
void ConnectionView::setOutputPort(PortView* portAddress){

    outputPortAdd = portAddress;
    outputPortAdd->getConnectionList().push_back(this);
}

/**
 * @brief ConnectionView::setInputPort setter metoda ; ulozi view vstupniho portu do pole outputPortAdd pro pristup do
 * logiky portu pri provadeni operaci.
 * @param portAddress adresa view vystupniho portu
 */
void ConnectionView::setInputPort(PortView *portAddress){

    inputPortAdd = portAddress;
    inputPortAdd->getConnectionList().push_back(this);
}

/**
 * @brief ConnectionView::updatePosFromPorts metoda se stara o zajisteni coord. uvnitr scene pri zmene pozici bloku
 */
void ConnectionView::updatePosFromPorts(){

    if(outputPortAdd){
        outputPortPos = outputPortAdd->scenePos();
    }

    if(inputPortAdd){
       inputPortPos = inputPortAdd->scenePos();
    }
}


/**
 * @brief ConnectionView::updatePath metoda prekresli propojeni mezi bloky
 * @see PortView.cpp
 */
void ConnectionView::updatePath(){

    QPainterPath painter;

    painter.moveTo(outputPortPos);

    qreal changeXPosition = inputPortPos.x() - outputPortPos.x();
    qreal changeYPosition = inputPortPos.y() - outputPortPos.y();

    QPointF newPosOutput(outputPortPos.x() + changeXPosition * 0.25, outputPortPos.y() + changeYPosition * 0.1);
    QPointF newPosInput(outputPortPos.x() + changeXPosition * 0.75, outputPortPos.y() + changeYPosition * 0.9);

    painter.cubicTo(newPosOutput, newPosInput, inputPortPos);

    setPath(painter);
}

/**
 * @brief ConnectionView::getOuputPortAdd getter metoda ; vrati adresu output portu
 * @return adresa output portu
 */
PortView* ConnectionView::getOuputPortAdd() const{

    return outputPortAdd;
}


/**
 * @brief ConnectionView::getInputAdd getter metoda ; vrati adresu input portu
 * @return adresa input portu
 */
PortView* ConnectionView::getInputPortAdd() const{

    return inputPortAdd;
}


/**
 * @brief ConnectionView::save uklada textovou reprezentaci propojeni do souboru
 * @param ds stream pro ulozeni text. rept. propoje
 */
void ConnectionView::save(std::ofstream& ds)
{
    ds << this->textRepresentation();
}


/**
 * @brief ConnectionView::pushData metoda preposle data ze backendu portu na jiny port pres propoj connection
 */
void ConnectionView::pushData(){

    inputPortAdd->portBackend->setData(outputPortAdd->portBackend->getData());
}



/**
 * @brief ConnectionView::hoverEnterEvent zpracuje event po najeti mysi nad propoj
 */
void ConnectionView::hoverEnterEvent(QGraphicsSceneHoverEvent*){

    this->connectionData = new QGraphicsTextItem(this);

    this->connectionData->setPlainText(QString::fromStdString("A: " + std::to_string(this->getOuputPortAdd()->portBackend->getData())));
    this->connectionData->setPos((outputPortPos.x() + inputPortPos.x())/2,((outputPortPos.y() + inputPortPos.y())/2) - 30);
}



/**
 * @brief ConnectionView::hoverLeaveEvent
 */
void ConnectionView::hoverLeaveEvent(QGraphicsSceneHoverEvent*){

    delete this->connectionData;
}

/**
 * @brief ConnectionView::textRepresentation vrati textovou reprezentaci propoje
 * @return retezec, reprezentujici propoj
 * @todo Vytvorit pretezovani operatoru << pro ulozeni do souboru
 */
std::string ConnectionView::textRepresentation(){

    std::string stringConnection = "--CONNECTION--\n" +
                                std::to_string(this->outputPortPos.x()) + "\n" +
                                std::to_string(this->outputPortPos.y()) + "\n" +
                                std::to_string(this->inputPortPos.x())+ "\n" +
                                std::to_string(this->inputPortPos.y())+ "\n" +
                                "--END CONNECTION--\n";

   return stringConnection;
}


/**
 * @brief ConnectionView::type Getter metoda ; vrati typ objektu uvnitr sceny
 * @return type objektu
 * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
 */
int ConnectionView::type() const{
      return Type;
  }
