/**
  @file InverterBlock.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro InverterBlock.cpp
  @see InverterBlock.cpp
*/

#ifndef INVERTERBLOCK_H
#define INVERTERBLOCK_H

#include "block.h"
#include <cmath>

class InverterBlock : public Block
{
public:

    /**
     * @brief InverterBlock::InverterBlock Primarni konstruktor tridy
     * @param inputPortsNum pocet vstupnich bloku invertoru (je vzdy 1)
     * @param outputPortsNum pocet vystupnich bloku invertoru ( je vzdy 1)
     */
    InverterBlock(const int inputPortsNum, const int outputPortsNum);

    /**
     * @brief InverterBlock::operation overreide abstraktni metody operation abstaktni tridy block. Invertuje hodnotu na opacnou v pripade aritmeticke operace.
     * Jinak invertuje bity.
     */
    void operation() override;

};
#endif // INVERTERBLOCK_H
