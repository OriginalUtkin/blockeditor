#include "block.h"

/**
  @file Block.cpp
  @author Utkin Kirill (xutkin00)
  @brief defenice tridy Block
*/

/**
 * @brief Block::Block Primarni konstruktor tridy Block
 * @param inputPortsNum pocet vstupnich portu bloku
 * @param outputPortsNum pocet vystupnich portu bloku
 */
Block::Block(const int inputPortsNum, const int outputPortsNum):inpPortsNum(inputPortsNum), outPortsNum(outputPortsNum)
{
    state = false; //pocatecni stav bloku

    if(inputPortsNum > 0){

        inputPorts = new Port[inpPortsNum]; // vytvoreni pole pro vstupnu porty

        for(int i = 0; i < inputPortsNum; ++i){

            inputPorts[i].setPortType(Input);
            inputPorts[i].setParent(this);
        }
    }

    if(outputPortsNum > 0){
        outputPorts = new Port[outPortsNum]; // vytvoreni pole pro vystupni porty

        for(int i = 0; i < outputPortsNum; ++i){

            outputPorts[i].setPortType(Output);
            outputPorts[i].setParent(this);
        }
    }
}


/**
 * @brief Block::changeState setter metoda ; nastavi pole state na opacnou hodnotu
 */
void Block::changeState(){

    state = !state;
}


/**
 * @brief Block::getState Vraci stav bloku
 * @return true pokud blok je aktivni a lze provest vypocet, jinak vraci false
 */
bool Block::getState() const{

    return state;
}


/**
 * @brief Block::~Block priparni destruktor
 */
Block::~Block(){
    std::cout << "BLOCK BACKEND DESTRUKTOR " << std::endl;
    delete[] inputPorts;
    delete[] outputPorts;
}


/**
 * @brief Block::setOutputPorts nastavuje hodnoty vystupnich portu bloku
 * @param value hodnota, kterou nutne ulozit do vystupnich portu bloku
 */
void Block::setOutputPorts(const double value){

    for(unsigned int i = 0; i < outPortsNum; i++){
        outputPorts[i].setData(value);
    }
}


/**
 * @brief Block::setData nastavuje hodnotu na vstupnich portech bloku
 * @param value hodnota, kterou je nutne ulozit do vstupnich portu
 */
void Block::setData(double value){

    this->data = value;
}


/**
 * @brief Block::getData funkce vrati hodnotu, ktera je ulozena na ve vstupnich portech bloku
 * @return hodnota typu double
 */
double Block::getData() const{

    return this->data;
}


/**
 * @brief Block::getBlockType getter metoda ; vrati typ bloku
 * @return hodnota enum definujici druh bloku
 * @see BlockType.h
 */
BlockType Block::getBlockType() const{

    return this->blockType;
}


/**
 * @brief Block::checkPorts metoda provadi kontrolu, zda-li vsichni porty maji hodnotu odlisnou od NaN
 * @return true, pokud vsichni porty maji nastavenou hodnotu, jinak vrati false
 */
bool Block::checkInputPorts() const{

    for(unsigned int counter = 0; counter < this->inpPortsNum; counter++){

        if(std::isnan(this->inputPorts[counter].getData()))
            return false;
    }

    return true;
}


/**
  * @brief Block::clearInputPorts metoda nastavuje vsichni vstupni porty na NaN
  */
 void Block::clearInputPorts(){

     for(unsigned int i = 0; i < inpPortsNum; i++){
         inputPorts[i].setData(std::numeric_limits<double>::quiet_NaN());
     }
 }


/**
 * @brief Block::getInputPort getter metoda ; metoda vrati adresu vstupniho portu
 * @param i ciiselny identifikator portu
 * @return adresa vstupniho portu
 */
Port* Block::getInputPort(unsigned int i){

    return this->inputPorts+i;
}


/**
 * @brief Block::getOutputPort getter metoda ; vrati adresu vystupniho portu
 * @param i ciiselny identifikator portu
 * @return adresa vystupniho portu
 */
Port* Block::getOutputPort(unsigned int i){

    return this->outputPorts+i;
}


/**
 * @brief Block::getOperationResult getter metoda ; vrati vysledek, ulozeny na vystupnich portech po provadeni vypocetne operaci
 * @return double hodnota - vysledek operaci bloku
 */
double Block::getOperationResult() const{

    return this->operationResult;
}


/**
 * @brief Block::setOpResult
 * @param value
 * @return
 */
void Block::setOpResult(double value){
    this->operationResult = value;
}


/**
 * @brief Block::intBlockType metoda prevadi enum hodnotu blockType na ciselnou hodnotu typu int pro ulozeni do load souboru
 * @return int hodnota reprezentujici typ bloku
 * @see BlockType
 */
int Block::intBlockType() const{

    switch (this->blockType){

    case InputBlockType:
        return 1;
        break;

    case OperationBlockType:
        return 2;
        break;

    case OutputBlockType:
        return 3;
        break;

    default:
        break;
    }
}


/**
 * @brief Block::loadBlock metoda nastavuje pole instanci na hodnoty ze load souboru
 * @param blockState stav bloku po ulozeni
 * @param type typ bloku
 * @param inputData data na vstupnich portech
 * @param outputData data na vystupnich portech
 */
void Block::loadBlock(const bool blockState, const BlockType type, const double blockOperationData, const double outputData, std::vector<double> inputPortsData){

    this->state = blockState;
    this->blockType = type;\

    if(this->inpPortsNum > 0){

        for(unsigned int counter = 0 ; counter < inputPortsData.size(); counter++){

            this->inputPorts[counter].setData(inputPortsData.at(counter));
        }
    }

    this->setData(blockOperationData);
    this->setOutputPorts(outputData);
}


