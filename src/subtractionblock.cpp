/**
  @file SubtractionBlock.cpp
  @author Utkin Kirill, xutkin00
  @brief logika pro Subtraction block
  @see Block.h
  @see SubtractionBlock.h
*/

#include "subtractionblock.h"


/**
 * @brief SubtractionBlock::SubtractionBlock Primarni konstruktor tridy
 * @param inputPortsNum pocet vstupnich portu
 * @param outputPortsNum pocet vystupnich portu
 */
SubtractionBlock::SubtractionBlock(const int inputPortsNum, const int outputPortsNum):Block(inputPortsNum,outputPortsNum){

    this->blockName = "Subtraction";
    blockType = OperationBlockType;

    this->data = std::numeric_limits<double>::quiet_NaN();
    this->operationResult = std::numeric_limits<double>::quiet_NaN();
}


/**
 * @brief SubtractionBlock::operation provadi vynasobeni dat ze vstupnich portu bloku a uklada vysledek do vystupnich portu
 */
void SubtractionBlock::operation() {

    if(!checkInputPorts()){
        this->data = std::numeric_limits<double>::quiet_NaN();

    }else{

        this->data = this->inputPorts[0].getData();

        for(unsigned int i = 1 ; i < inpPortsNum; ++i){
                this->data -= inputPorts[i].getData();
        }

        this->operationResult = this->data;
    }

    setOutputPorts(this->operationResult);
}

