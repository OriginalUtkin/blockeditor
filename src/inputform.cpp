/**
  @file InputForm.cpp
  @author Utkin Kirill, xutkin00
  @brief vytvari okenko pro vkladani dat do Input block (GUI)
  @see InputForm.ui
*/

#include "inputform.h"
#include "ui_inputform.h"


/**
 * @brief inputForm::inputForm konstruktor pro vytvoreni okenka pro ulozani vstupu do input bloku
 * @param parent
 * @param blockAddress adsresa bloku, kam je nutne ulozit pocatecni hodnotu
 */
inputForm::inputForm(QWidget *parent, BlockView* blockAddress) : QWidget(parent), ui(new Ui::inputForm){

    ui->setupUi(this);
    this->setWindowTitle("Input value");
    this->block = blockAddress;
    connect(ui->pushButton, SIGNAL(clicked()),this, SLOT(setUserValue()));
    ui->label->setText(QString::number(block->blockBackend->getData()));
}


/**
 * @brief inputForm::~inputForm destruktor formy
 */
inputForm::~inputForm(){

    this->block->blockBackend = nullptr;
    delete ui;
}


/**
 * @brief inputForm::setUserValue nastavuje hodnotu do input bloku
 */
void inputForm::setUserValue(){

    this->block->blockBackend->setData(ui->doubleSpinBox->value());
    this->close();
}
