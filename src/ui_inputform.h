/********************************************************************************
** Form generated from reading UI file 'inputform.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INPUTFORM_H
#define UI_INPUTFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_inputForm
{
public:
    QPushButton *pushButton;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *inputForm)
    {
        if (inputForm->objectName().isEmpty())
            inputForm->setObjectName(QStringLiteral("inputForm"));
        inputForm->resize(229, 104);
        pushButton = new QPushButton(inputForm);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 50, 89, 25));
        doubleSpinBox = new QDoubleSpinBox(inputForm);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(10, 10, 211, 26));
        doubleSpinBox->setDecimals(5);
        doubleSpinBox->setMaximum(1e+10);
        doubleSpinBox->setValue(0);
        label = new QLabel(inputForm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(120, 80, 81, 20));
        label_2 = new QLabel(inputForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 80, 111, 20));

        retranslateUi(inputForm);

        QMetaObject::connectSlotsByName(inputForm);
    } // setupUi

    void retranslateUi(QWidget *inputForm)
    {
        inputForm->setWindowTitle(QApplication::translate("inputForm", "Form", 0));
        pushButton->setText(QApplication::translate("inputForm", "Set value", 0));
        label->setText(QApplication::translate("inputForm", "TextLabel", 0));
        label_2->setText(QApplication::translate("inputForm", "Current value :", 0));
    } // retranslateUi

};

namespace Ui {
    class inputForm: public Ui_inputForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INPUTFORM_H
