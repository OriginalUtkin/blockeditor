/**
  @file ErrorForm.cpp
  @author Utkin Kirill, xutkin00
  @date 04 April 2018
  @brief Vytvoreni okenka, obsahujiciho chybove hlaseni (Error form class)
  @see ErrorForm.ui
*/

#include "errorform.h"
#include "ui_errorform.h"

/**
 * @brief inputForm::ErrorForm konstruktor pro vytvoreni okenka pro vypis chyboveho hlaseni behem simulace
 * @param parent
 */
ErrorForm::ErrorForm(QWidget *parent) : QWidget(parent), ui(new Ui::ErrorForm){

    ui->setupUi(this);
    this->setWindowTitle("Error message");
    ui->label_2->setText(QString::fromStdString("Porty nejsou propojeny nebo alespon jeden vstup nema pocatacnou hodnotu"));
}

/**
 * @brief ErrorForm::~ErrorForm primarni destruktor tridy
 */
ErrorForm::~ErrorForm(){

    delete ui;
}
