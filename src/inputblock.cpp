/**
  @file InputBlock.cpp
  @author Utkin Kirill, xutkin00
  @brief Block Input, ktery zjiskava data od uzivatele a prevadi na vystupni porty
  @see Block.h
*/

#include "inputblock.h"

/**
 * @brief InputBlock::InputBlock konstruktor tridy InputBlock. Implicitne uklada do pole inputData hodnotu NaN
 * @param inputPortsNum pocet vsupnich portu bloku (musi byt nula pro vstupni port)
 * @param outputPortsNum pocet vystupnich portu bloku
 */
InputBlock::InputBlock(const int inputPortsNum, const int outputPortsNum):Block(inputPortsNum, outputPortsNum)
{
    this->data = std::numeric_limits<double>::quiet_NaN();
    this->operationResult = std::numeric_limits<double>::quiet_NaN();

    blockType = InputBlockType;
    this->blockName = "Input";
    changeState(); // Nastav stav bloku na True ( Muze provest operaci )
}


/**
 * @brief InputBlock::operation nastavuje vystupni porty inputBloku
 */
void InputBlock::operation(){
    std::cout << "OPERATION INPUT" <<std::endl;
    std::cout << this->data << std::endl;
    this->operationResult = this->data;
    setOutputPorts(operationResult);
}
