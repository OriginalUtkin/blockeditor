/**
  @file BlockType.h
  @author Utkin Kirill, xutkin00
  @brief Defenuje typ bloku
*/

#ifndef BLOCKTYPE_H
#define BLOCKTYPE_H

typedef enum{

    InputBlockType, // defunuje vstupni blok
    OperationBlockType, // definuje blok, ktery provadi vypocetni op.
    OutputBlockType // defenuje vystupni blok

}BlockType ;

#endif // BLOCKTYPE_H
