/********************************************************************************
** Form generated from reading UI file 'outputform.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OUTPUTFORM_H
#define UI_OUTPUTFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OutputForm
{
public:
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *OutputForm)
    {
        if (OutputForm->objectName().isEmpty())
            OutputForm->setObjectName(QStringLiteral("OutputForm"));
        OutputForm->resize(240, 62);
        label = new QLabel(OutputForm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 20, 131, 16));
        label_2 = new QLabel(OutputForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(130, 20, 111, 17));

        retranslateUi(OutputForm);

        QMetaObject::connectSlotsByName(OutputForm);
    } // setupUi

    void retranslateUi(QWidget *OutputForm)
    {
        OutputForm->setWindowTitle(QApplication::translate("OutputForm", "Form", 0));
        label->setText(QApplication::translate("OutputForm", "Calculation result:", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class OutputForm: public Ui_OutputForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OUTPUTFORM_H
