/**
  @file Parser.cpp
  @author Utkin Kirill, xutkin00
  @brief Parsuje load soubor a vrati jednotlive polozky
*/

#include "parser.h"

/**
 * @brief Parser::Parser Implicitni kostruktor tridy
 */
Parser::Parser()
{

}


/**
 * @brief Parser::getParseData zjiskava polozku z load souboru pro parsovani. Pouziva dalsi pomocne funkci
 * @param fileLines obsah load souboru
 * @return struct hodnotu , potrebnou pro vytvoreni objektu ci propojeni
 * @see blockData.h
 */
blockData Parser::getParseData(std::string &fileLines){

    blockData result;
    std::vector<std::string> parsedData = parser(fileLines);
    result = convertData(parsedData);

    return result;
}

/**
 * @brief Parser::parser vrati polozku ve formatu vector<string>
 * @param fileLines obsah load souboru
 * @return jednu polozku ve formatu vector<string>
 */
std::vector<std::string> Parser::parser(std::string& fileLines){

    std::string line;
    unsigned int count = 0;
    std::istringstream stringStream(fileLines);
    std::vector<std::string> parsedData;

    while (std::getline(stringStream, line)) {

        count+=1;

        if( (line == "--BLOCK--") || (line == "--CONNECTION--") ){
            parsedData.push_back(line);
            continue;
        }

        else if( (line == "--END BLOCK--") || (line == "--END CONNECTION--") ){
            break;
        }

        else
            parsedData.push_back(line);
    }

    for(unsigned int counter = 0; counter < count; counter++){

        unsigned int lastPos = fileLines.find('\n',0);
        fileLines.erase(0, lastPos + 1);
    }

    return parsedData;
}


/**
 * @brief Parser::convertData konvertuje data z vector<string> na blockData
 * @param data polozka z load souboru veformatu vector<string>
 * @return polozku BlockData pro vytvoreni objektu ci propojeni
 * @see BlockData.h
 */\
blockData Parser::convertData(const std::vector<std::string>& data){

    blockData result;

    if(data.at(0) == "--BLOCK--"){ // Polozka obsahuje blok

        result.objectType = "Block";
        result.blockName = data.at(1);

        result.blockState = to_bool(data.at(2));
        result.blockType = std::stoi(data.at(3));

        result.inputPortNum = std::stoi(data.at(4));
        result.outputPortNum = std::stoi(data.at(5));

        result.inputData = inputValueParse(data.at(6));

        result.dataForBlock = std::stod(data.at(7));
        result.outputData = std::stod(data.at(8));

        result.xScenePosition = std::stod(data.at(9));
        result.yScenePosition = std::stod(data.at(10));

    }else{ // polozka obsahuje propojeni

        result.objectType = "Connection";
        result.xScenePosition = std::stod(data.at(1));
        result.yScenePosition = std::stod(data.at(2));
        result.xIScenePosition = std::stod(data.at(3));
        result.yIScenePosition= std::stod(data.at(4));
    }

    return result;
}


/**
 * @brief Parser::to_bool prevadi textovou reprezentaci do bool
 * @param str retezec pro prevod
 * @return vrati true v pripade ze hodnota ze str je true, jinak vrati false
 */
bool Parser::to_bool(std::string str){

    std::istringstream converter(str); // prevod retezce na stream
    bool result;

    converter >> std::boolalpha >> result;

    return result;
}


/**
 * @brief Parser::inputValueParse
 * @param inputValueString
 * @return
 */
std::vector<double> Parser::inputValueParse(const std::string& inputValueString){

    std::string inputValueStringCopy = inputValueString;
    std::vector<double> resultVector;
    inputValueStringCopy.erase(0, 6);

    if(inputValueStringCopy.empty()){
        return std::vector<double>();

    }else{

        while(!inputValueStringCopy.empty()){
            std::size_t found = inputValueStringCopy.find('>');
            std::string value = inputValueStringCopy.substr(1, found - 1);
            resultVector.push_back(stod(value));
            inputValueStringCopy.erase(0, found + 1);
        }
    }

    return resultVector;
}
