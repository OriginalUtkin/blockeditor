/**
  @file MultiplicationBlock.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro MultiplicationBlock.cpp
  @see MultiplicaationBlock.cpp
*/

#ifndef MULTIPLICATIONBLOCK_H
#define MULTIPLICATIONBLOCK_H

#include "block.h"
#include "cmath"

class MultiplicationBlock : public Block
{
public:


    /**
     * @brief MultiplicationBlock::MultiplicationBlock Primarni konstruktor tridy
     * @param inputPortsNum pocet vstupnich portu
     * @param outputPortsNum pocet vystupnich portu
     */
    MultiplicationBlock(const int inputPortsNum, const int outputPortsNum);


    /**
     * @brief MultiplicationBlock::operation overreide abstraktni metody operation ze abstaktni tridy block. Vynasobi cisla ze vstupnich portu.
     */
    void operation() override;
};
#endif // MULTIPLICATIONBLOCK_H
