/**
  @file BlockFactory.h
  @author Utkin Kirill, xutkin00
  @brief Popisuje tovarnu pro vytvoreni objektu
  @see BlockFactory.cpp
*/

#ifndef BLOCKFACTORY_H
#define BLOCKFACTORY_H
#include "block.h"

class BlockFactory
{
public:
    BlockFactory();
    virtual Block* createBackend(const unsigned int inputPortNum, const unsigned int outputPortNum) = 0;
    virtual ~BlockFactory();

};

#endif // BLOCKFACTORY_H
