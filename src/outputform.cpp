/**
  @file OutputForm.cpp
  @author Utkin Kirill, xutkin00
  @brief Okenko pro vypis vysledku z Output block
  @see OutputBlock;
*/

#include "outputform.h"
#include "ui_outputform.h"


/**
 * @brief OutputForm::OutputForm primarni konstruktor
 * @param resultValue vysledek pr ozobrazeni
 * @param parent
 */
OutputForm::OutputForm(const double resultValue, QWidget *parent) : QWidget(parent), ui(new Ui::OutputForm){

    ui->setupUi(this);
    this->setWindowTitle("Output result");
    ui->label_2->setText(QString::fromStdString(std::to_string(resultValue)));
}

/**
 * @brief OutputForm::~OutputForm destruktor tridy
 */
OutputForm::~OutputForm(){

    delete ui;
}
