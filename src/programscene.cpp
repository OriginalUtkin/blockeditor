/**
  @file ProgramScene.cpp
  @author Utkin Kirill, xutkin00
  @brief Main program scene
*/

#include "programscene.h"

/**
 * @brief ProgramScene::ProgramScene Primarni konstruktor
 * @param parent
 */
ProgramScene::ProgramScene(QObject *parent) : QObject(parent){

}

/**
 * @brief ProgramScene::setScene metoda nastavuje event filter pro scene
 * @param s scene objekt, vytvoreny pri spusteni programu
 * @see http://doc.qt.io/qt-5/qobject.html#installEventFilter
 */
void ProgramScene::setScene(QGraphicsScene *s){

    s->installEventFilter(this);

    scene = s;
}


/**
 * @brief ProgramScene::itemAt metoda zajistuje jake objekty nachazeji na pozici pos
 * @param pos pozice ; coord. ziskavaji po stisknuti tlacitka mysi
 * @return seznam objektu, kteri nachazeji na pozici pos
 */
QGraphicsItem* ProgramScene::itemAt(const QPointF &pos){

    QList<QGraphicsItem*> items = scene->items(QRectF(pos - QPointF(1,1), QSize(3,3)));

    foreach(QGraphicsItem *item, items){

        if (item->type() > QGraphicsItem::UserType)
            return item;
    }

    return 0;
}


/**
 * @brief ProgramScene::eventFilter zpracuje eventy ze scene a dovoluje iterakci s objekty uvnitr scene
 * @param o
 * @param mouseEvent event od mysi
 * @return true v pripade zpracovani eventu
 * @see http://doc.qt.io/qt-5/eventsandfilters.html
 */
bool ProgramScene::eventFilter(QObject *object, QEvent *mouseEvent){

    QGraphicsSceneMouseEvent *mouseButtonEvent = (QGraphicsSceneMouseEvent*) mouseEvent;

    int eventType = (int) mouseEvent->type(); // zajisteni typu eventu

        if(eventType == QEvent::GraphicsSceneMousePress){ // stisknuti tlacitka

            int mButtonEvent = (int) mouseButtonEvent->button();

                if(mButtonEvent ==  Qt::LeftButton){

                    QGraphicsItem *item = itemAt(mouseButtonEvent->scenePos());

                    if (item && item->type() == PortView::Type){

                        potentialConnection = new ConnectionView(nullptr);
                        scene->addItem(potentialConnection);
                        potentialConnection->setOutputPort((PortView*) item);
                        potentialConnection->setOutpuPortPos(item->scenePos());
                        potentialConnection->setInputPortPos(mouseButtonEvent->scenePos());
                        potentialConnection->updatePath();

                        return true;
                    }

                    return false;
                }

                if(mButtonEvent == Qt::RightButton){

                    QGraphicsItem *item = itemAt(mouseButtonEvent->scenePos());

                    if (item && (item->type() == ConnectionView::Type || item->type() == BlockView::Type))
                        delete item;

                    return true;
                }

                return false;
        }

        if(eventType == QEvent::GraphicsSceneMouseMove){ // pretahovani

            if (potentialConnection){

                potentialConnection->setInputPortPos(mouseButtonEvent->scenePos());
                potentialConnection->updatePath();
                return true;
            }

            return false;
        }

        if(eventType == QEvent::GraphicsSceneMouseRelease){ // Opousteni tlacitka

            int mButtonEvent = (int) mouseButtonEvent->button();

            if (potentialConnection && mButtonEvent == Qt::LeftButton){

                QGraphicsItem *item = itemAt(mouseButtonEvent->scenePos());

                if (item && item->type() == PortView::Type){

                    PortView *outputPort = potentialConnection->getOuputPortAdd();
                    PortView *inputPort = (PortView*) item;

                    if (outputPort->portBackend->getParent() != inputPort->portBackend->getParent() && outputPort->portBackend->getPortType() != inputPort->portBackend->getPortType() &&
                        !outputPort->isConnected(inputPort) && !outputPort->portBackend->isConnected()){ // Podariso vytvorit propojeni mezi bloky

                        potentialConnection->setInputPortPos(inputPort->scenePos());
                        potentialConnection->setInputPort(inputPort);

                        potentialConnection->outputPortAdd->portBackend->setConnected(true);
                        potentialConnection->inputPortAdd->portBackend->setConnected(true);

                        potentialConnection->updatePath();

                        potentialConnection = nullptr;

                        return true;
                    }
                }

                delete potentialConnection; // nepodarilo vytvorit spojeni
                potentialConnection = nullptr;

                return true;
            }

            return false;
        }

        if(eventType ==  QEvent::GraphicsSceneMouseDoubleClick){

                QGraphicsItem *item = itemAt(mouseButtonEvent->scenePos());

                if (item && item->type() == BlockView::Type && ((BlockView*)item)->blockBackend->getBlockType() == InputBlockType){

                    this->myInputForm.push_back( new inputForm(0, (BlockView*)item));
                    //this->myInputForm.at(this->myInputForm.size()-1)->setAttribute(Qt::WA_DeleteOnClose, true); <--- SIGFAULT here
                    this->myInputForm.at(this->myInputForm.size()-1)->show();

                    return true;
                }

                if (item && item->type() == BlockView::Type && ((BlockView*)item)->blockBackend->getBlockType() == OutputBlockType){

                    this->outputForms.push_back( new OutputForm(((BlockView*)item)->blockBackend->getData(), 0));
                    this->outputForms.at(this->outputForms.size()-1)->show();
                    //this->myInputForm.at(this->myInputForm.size()-1)->setAttribute(Qt::WA_DeleteOnClose, true); <--- SIGFAULT here

                    return true;
                }
        }

    return QObject::eventFilter(object, mouseEvent);
}

/**
 * @brief ProgramScene::save ulozeni obsahu sceny do souboru
 * @param ds stream pro ulozeni sceny do souboru
 */
void ProgramScene::save(std::ofstream &ds){

    //for(int k=0, s=scene->items().size(), max=(s/2); k<max; k++) scene->items().swap(k,s-(1+k));
    // ulozeni vsech bloku
    for(int count = scene->items().size() - 1; count >= 0; count--){
        if(scene->items().at(count)->type() == BlockView::Type){
            ((BlockView*) scene->items().at(count))->save(ds);
        }
    }

    // ulozeni propoje(vzdy az na konci)

    foreach(QGraphicsItem *item, scene->items())

        if (item->type() == ConnectionView::Type){

            ((ConnectionView*) item)->save(ds);
        }
}

/**
 * @brief ProgramScene::reloadScene metoda vycisti scenu pred nahranim souboru
 * @todo je nutne smazat objekty
 */
void ProgramScene::reloadScene(){
    scene->clear();
}
