/**
  @file Connecton.cpp
  @author Utkin Kirill, xutkin00
  @brief Class represents connection between blocks in sheme
*/

#include "connection.h"


/**
 * @brief Connection::Connection
 * @param outputPort
 * @param inputPort
 */
Connection::Connection(Port *outputPort, Port *inputPort)
{
    outputPortAdd = outputPort;
    inputPortAdd = inputPort;

    outputPortAdd->setConnected(true);
    inputPortAdd->setConnected(true);
}


/**
 * @brief Connection::~Connection
 */
Connection::~Connection(){

    this->outputPortAdd->setConnected(false);
    this->inputPortAdd->setConnected(false);

    this->outputPortAdd = nullptr;
    this->inputPortAdd = nullptr;
}


/**
 * @brief Connection::getOutputAddress
 * @return
 */
Port *Connection::getOutputAddress() const{
    return this->outputPortAdd;
}


/**
 * @brief Connection::getInputAddress
 * @return
 */
Port* Connection::getInputAddress() const{
    return this->inputPortAdd;
}


/**
 * @brief Connection::pushData
 */
void Connection::pushData(){
    this->inputPortAdd->setData(this->outputPortAdd->getData());
}
