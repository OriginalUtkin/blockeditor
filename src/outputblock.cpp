/**
  @file OutputBlock.cpp
  @author Utkin Kirill, xutkin00
  @brief Obsahuje vysledek po vypoctu
*/

#include "outputblock.h"


/**
 * @brief OutputBlock::OutputBlock Primarni konstruktor tridy
 * @param inputPortsNum pocet vstupnich portu ( je vzdy 1)
 * @param outputPortsNum pocet vystupnich portu (je vzdy 0)
 */
OutputBlock::OutputBlock(const int inputPortsNum, const int outputPortsNum):Block(inputPortsNum, outputPortsNum){

    this->blockName = "Output";
    this->data = std::numeric_limits<double>::quiet_NaN();
    blockType = OutputBlockType;
}


/**
 * @brief OutputBlock::operation overreide abstraktni metody operation ze abstaktni tridy block. Uklada hodnoty ze vstupniho portu do pole Data pro zobrazeni vysledku.
 */
void OutputBlock::operation(){
    std::cout << "OPERATION OUTPUT"<< std::endl;
    this->data = this->inputPorts[0].getData();
}

