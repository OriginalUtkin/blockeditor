/**
  @file Port.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro Port.cpp
  @see Port.cpp
  @see PortView.cpp
*/

#ifndef PORT_H
#define PORT_H

#include <limits>
#include <iostream>
#include "datatype.h"
#include "porttype.h"
#include "portconnection.h"

class Block;

class Port
{
public:

    /**
     * @brief Port::Port implicitni konstruktor tridy. Po vytvoreni objektu nastavuje pole data na hodnotu NaN
     */
    Port();

    /* Set methods */

    /**
     * @brief Port::setDataType getter metoda; nastavuje pole dataType na hodnotu type
     * @param type DataType hodnota, reprezentujici data v portu
     * @see DataType
     */
    void setDataType(DataType type);


    /**
     * @brief Port::setPortType setter metoda ; nastavi typ portu na hodnotu type
     * @param type typ portu
     * @see PortType
     */
    void setPortType(PortType type);


    /**
     * @brief Port::setDatanasavuje Data pro predani do bloku. Bude pak pouzita pro provedeni operaci uvnitr bloku
     * @param value data
     */
    void setData(double value);


    /**
     * @brief Port::setParent getter metoda ; nastavuje pole parent
     * @param parentBlock ukazatel na Block
     * @see Block.h
     */
    void setParent(const Block *parentBlock);


    /**
     * @brief Port::setConnected setter metoda; nastavi pole connected na hodnotu value
     * @param value hodnota typu bool. Nastavuje na true, pokud port je pripojen, jinak false
     */
    void setConnected(bool value);

    /* Get methods */

    /**
     * @brief Port::getDataType getter metoda ; vrati typ dat, kteri jsou ulozene v portu
     * @return hodnotu pole dataType
     * @see PortData.h
     */
    DataType getDataType() const;


    /**
     * @brief Port::getPortType getter metoda ;  Vrati typ portu
     * @return typ portu
     * @see PortType
     */
    PortType getPortType() const;


    /**
     * @brief Port::getData getter metoda ; vrati double hodnotu z pole Data prislusneho portu
     * @return double hodnota
     */
    double getData() const;


    /**
     * @brief Port::isConnected getter metoda, vrati hodnotu z pole connected prislusneho portu
     * @return true pokud port ve stavu "Pripojen", jinak vrati false
     */
    bool isConnected() const;


    /**
     * @brief Port::getParent getter metoda, vrati odkaz na Blok, ktery tento port obdrzuje
     * @return Block* hodnotu
     */
    const Block *getParent() const;


private:

    static const std::string EnumPortType[];
    static const std::string EnumDataType[];

    /**
     * @brief Port::getPortTypeString prevadi hodnotu EnumPortValue do String
     * @param enumValue int hodnota reprezentujici hodnotu z enum seznamu
     * @return odpovidajici polozka z enum seznamu ve string formatu
     */
    std::string getPortTypeString(int enumValue) const;

    DataType dataType; //typ data v portu
    PortType portType; // typ portu
    bool connected = false; //stav portu
    double data; // vnitrni stav portu
    const Block*  parent; //odkaz na rodicovsku block


    /**
     * @brief operator << pretizena operace pro vytvoreni textove reprezentaci portu
     * @param oshreference do OutputStream
     * @param port reference to port
     * @return reference to OutputStream
     */
    friend std::ostream& operator << (std::ostream& os, const Port& port);

};

#endif // PORT_H
