/**
  @file Model.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro Model.cpp
  @see Model.cpp
*/

#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <algorithm>
#include <cmath>
#include <QVector>

#include "connection.h"
#include "block.h"
#include "additionalblock.h"
#include "inputblock.h"
#include "outputblock.h"
#include "subtractionblock.h"
#include "inverterblock.h"
#include "multiplicationblock.h"
#include "portview.h"
#include "blockview.h"
#include "portview.h"
#include "connectionview.h"


class Model
{
public:

    /**
     * @brief Model::Model prazany primarni konstruktor pro vytvoreni vypocetniho modelu
     */
    Model();


    /**
     * @brief Model::simulationStep jeden krok vypocetniho modelu ktery provadi operaci ve vsech aktivnich blocich
     * @return true, pokud existuje dalsi krok vypocetniho modelu, jinak vraci false
     */
    bool simulationStep();


    /**
     * @brief Model::simulationCheck metoda pro kontrolu spravnosti sestaveni schemata
     * @return true, pokud lze provest vypocet, jinak vrati false
     */
    bool simulationCheck() const;


    /**
     * @brief resetSimulationState nastavi hodnotu pole SimulationState na hodnotu true
     */
    void resetSimulationState();


    /**
     * @brief Model::addBlock pridava blok do vypocetniho modelu. Funkce se zavola po vytvoreni bloku
     * @param value adresa backendu bloku, ktery je nutne pridat do vypocetniho systemu
     */
    void addBlock(Block* value);
    void removeBlock(Block* value);
    void addConnection(Connection* value);
    void remove(Connection* value);

    // TODO : ADD TO PRIVATE
    QVector<ConnectionView*> all_connections;
    QVector<ConnectionView*> closedConnection;


    /**
     * @brief Model::getSimulationState getter, ktery vrati stav vypocetniho modelu
     * @return true, pokud je mozne provadet vypocet dal, jinak false , ktery signalizuje ze vypocet ukoncen
     */
    bool getSimulationState() const;

    std::vector<Block*> allBlocks;

     std::vector<Connection*> allConnections;
     std::vector<const Block*> activeBlocks, nextBlocks;

//TODO -> Спрятать все переменные в private
private:

    // Metody vypocetneho modelu
     /**
      * @brief Model::setSimulationStepData nastavuje data pro vypocetny krok. Nastavuje bloky pro dalsi iteraci
      */
    void setSimulationStepData();


    /**
     * @brief Model::blockOperation provadi operaci aktivniho bloku
     * @see Block.Oeration
     */
    void blockOperation();


    /**
     * @brief Model::pushDataToBlock provadi prevod dat z aktivniho bloku do input portu dalsiho(seznam nextBlocks) bloku
     * @see Connection.pushData()
     */
    void pushDataToBlock();


    /**
     * @brief Model::changeToPassive provadi blokovani bloku po porvedeni operaci
     * @see block.operation()
     */
    void changeToPassive();


    /**
     * @brief Model::eraseBlockFromNext vyhodi blok ze seznamu bloku pro dalsi vypocetni iteraci
     */
    void eraseBlockFromNext();


    /**
     * @brief Model::activateBlocks nastavuje stav bloku na Active pro vypocet v dalsim kroku vypocetniho modelu
     */
    void activateBlocks();


    /**
     * @brief Model::clearSimulationData provadi operaci vyprazdneni vsech elementu ze seznamu activeBlocks a nextBlocks
     */
    void clearSimulationData();

    // Stavove promenne vypocetneho modelu
    bool simulationState = true;
};

#endif // MODEL_H
