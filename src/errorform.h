/**
  @file ErrorForm.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro okenko, obsahujici popis chyby
  @see ErrorForm.cpp
*/

#ifndef ERRORFORM_H
#define ERRORFORM_H

#include <QWidget>

namespace Ui {
class ErrorForm;
}

class ErrorForm : public QWidget
{
    Q_OBJECT

public:
    explicit ErrorForm(QWidget *parent = nullptr);
    ~ErrorForm();

private:
    Ui::ErrorForm *ui;
};

#endif // ERRORFORM_H
