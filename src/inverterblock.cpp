/**
  @file InverterBlock.cpp
  @author Utkin Kirill, xutkin00
  @brief Invertuje hodnotu vstupu na opacnou a ulozi na vystupni porty
  @see Block.h
*/

#include "inverterblock.h"

/**
 * @brief InverterBlock::InverterBlock Primarni konstruktor tridy
 * @param inputPortsNum pocet vstupnich bloku invertoru (je vzdy 1)
 * @param outputPortsNum pocet vystupnich bloku invertoru ( je vzdy 1)
 */
InverterBlock::InverterBlock(const int inputPortsNum, const int outputPortsNum):Block(inputPortsNum,outputPortsNum){

    this->blockName = "Inverter";
    blockType = OperationBlockType;

    this->data = std::numeric_limits<double>::quiet_NaN();
    this->operationResult = std::numeric_limits<double>::quiet_NaN();

}


/**
 * @brief InverterBlock::operation overreide abstraktni metody operation abstaktni tridy block. Invertuje hodnotu na opacnou v pripade aritmeticke operace.
 * Jinak invertuje bity.
 */
void InverterBlock::operation() {

    if(!checkInputPorts()){

        this->data = std::numeric_limits<double>::quiet_NaN();

    }else{

        this->data = -inputPorts[0].getData();
        this->operationResult = this->data;

        setOutputPorts(this->operationResult);
    }
}
