/********************************************************************************
** Form generated from reading UI file 'setportsform.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETPORTSFORM_H
#define UI_SETPORTSFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetPortsForm
{
public:
    QLabel *label;
    QSpinBox *spinBox;
    QSpinBox *spinBox_2;
    QLabel *label_2;
    QPushButton *pushButton;

    void setupUi(QWidget *SetPortsForm)
    {
        if (SetPortsForm->objectName().isEmpty())
            SetPortsForm->setObjectName(QStringLiteral("SetPortsForm"));
        SetPortsForm->resize(240, 140);
        label = new QLabel(SetPortsForm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 67, 31));
        spinBox = new QSpinBox(SetPortsForm);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(110, 10, 111, 26));
        spinBox->setMinimum(1);
        spinBox->setMaximum(5);
        spinBox->setValue(2);
        spinBox_2 = new QSpinBox(SetPortsForm);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setGeometry(QRect(110, 54, 111, 26));
        spinBox_2->setMinimum(1);
        spinBox_2->setMaximum(5);
        label_2 = new QLabel(SetPortsForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 50, 67, 31));
        pushButton = new QPushButton(SetPortsForm);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(70, 100, 89, 25));

        retranslateUi(SetPortsForm);

        QMetaObject::connectSlotsByName(SetPortsForm);
    } // setupUi

    void retranslateUi(QWidget *SetPortsForm)
    {
        SetPortsForm->setWindowTitle(QApplication::translate("SetPortsForm", "Form", 0));
        label->setText(QApplication::translate("SetPortsForm", "Inputs :", 0));
        label_2->setText(QApplication::translate("SetPortsForm", "Outputs :", 0));
        pushButton->setText(QApplication::translate("SetPortsForm", "Set", 0));
    } // retranslateUi

};

namespace Ui {
    class SetPortsForm: public Ui_SetPortsForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETPORTSFORM_H
