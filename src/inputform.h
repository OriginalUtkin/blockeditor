/**
  @file InputForm.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro InputForm.cpp
  @see InputForm.cpp
*/

#ifndef INPUTFORM_H
#define INPUTFORM_H

#include <QWidget>
#include "blockview.h"
#include "block.h"

namespace Ui {
class inputForm;
}

class inputForm : public QWidget
{
    Q_OBJECT

public:
    explicit inputForm(QWidget *parent = 0, BlockView*  blockAddress=nullptr);
    ~inputForm();

private:
    Ui::inputForm *ui;
    BlockView* block = nullptr;

private slots:
    void setUserValue();
};

#endif // INPUTFORM_H
