/**
  @file Connecton.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro Connection.cpp
  @see Connection.cpp
*/

#ifndef CONNECTION_H
#define CONNECTION_H

#include "port.h"
#include "block.h"

class Connection
{
public:
    Connection(Port* outputPort, Port* inputPort);
    ~Connection();

    Port* getOutputAddress() const ;
    Port* getInputAddress() const;
    void pushData();

private:
    Port* outputPortAdd;
    Port* inputPortAdd;
};

#endif // CONNECTION_H
