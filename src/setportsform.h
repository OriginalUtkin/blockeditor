/**
  @file SetPortsForm.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro SetPortsForm.cpp
  @see SetPortsForm.cpp
*/

#ifndef SETPORTSFORM_H
#define SETPORTSFORM_H

#include <QWidget>

namespace Ui {
class SetPortsForm;
}

class SetPortsForm : public QWidget
{
    Q_OBJECT

public:
    explicit SetPortsForm(unsigned int* inputValue, unsigned int* outputValue, QWidget *parent = 0);
    ~SetPortsForm();

private:
    Ui::SetPortsForm *ui;
    unsigned int* inputPortsNum = nullptr;
    unsigned int* outputPortsNum = nullptr;

private slots:
    void setUserValue();
};

#endif // SETPORTSFORM_H
