/**
  @file AdditionalFactory.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro AdditionalFactory.cpp
  @see AdditionalBlockFactory.cpp
*/

#ifndef ADDITIONALBLOCKFACTORY_H
#define ADDITIONALBLOCKFACTORY_H

#include "blockfactory.h"
#include "block.h"
#include "additionalblock.h"

class AdditionalBlockFactory : public BlockFactory
{
public:
    AdditionalBlockFactory();
    Block* createBackend(const unsigned int inputPortNum, const unsigned int outputPortNum) override;
};

#endif // ADDITIONALBLOCKFACTORY_H
