/**
  @file OutputBlock.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro OutputBlock.cpp
*/

#ifndef OUTPUTBLOCK_H
#define OUTPUTBLOCK_H

#include "block.h"

class OutputBlock : public Block
{
public:

    /**
     * @brief OutputBlock::OutputBlock Primarni konstruktor tridy
     * @param inputPortsNum pocet vstupnich portu ( je vzdy 1)
     * @param outputPortsNum pocet vystupnich portu (je vzdy 0)
     */
    OutputBlock(const int inputPortsNum, const int outputPortsNum);


    /**
     * @brief OutputBlock::operation overreide abstraktni metody operation ze abstaktni tridy block. Uklada hodnoty ze vstupniho portu do pole Data pro zobrazeni vysledku.
     */
    void operation() override ;
};

#endif // OUTPUTBLOCK_H
