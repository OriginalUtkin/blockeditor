/**
  @file AdditionalBlock.cpp
  @author Utkin Kirill, xutkin00
  @brief Blok Additional (summator)
  @see Block.h
*/

#include "additionalblock.h"

/**
 * @brief AdditionBlock::AdditionBlock primartni konstruktor. Vytvori block a priradi prislusne jmeno. Implicitne nastavi hodnotu
 * Data na hodnotu NaN
 * @param inputPortsNum pocet vstupnich portu
 * @param outputPortsNum pocet vystupnich portu bloku
 */
AdditionBlock::AdditionBlock(const int inputPortsNum, const int outputPortsNum):Block(inputPortsNum,outputPortsNum)
{
    blockType = OperationBlockType;
    this->blockName = "Addblock";

    this->data = std::numeric_limits<double>::quiet_NaN();
    this->operationResult = std::numeric_limits<double>::quiet_NaN();
}


/**
 * @brief AdditionBlock::operation Operaci scitani dat a nastavuje vystupni porty na hodnotu data
 */
void AdditionBlock::operation() {

    if(!checkInputPorts()){

        this->data = std::numeric_limits<double>::quiet_NaN();

    }else{

        this->data = 0;

        for(unsigned int i = 0 ; i < inpPortsNum; ++i){
                this->data += inputPorts[i].getData();
        }

        this->operationResult = this->data;
    }

    setOutputPorts(this->operationResult);
}

