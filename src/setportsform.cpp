/**
  @file SetPortsForm.cpp
  @author Utkin Kirill, xutkin00
*/

#include "setportsform.h"
#include "ui_setportsform.h"


/**
 * @brief SetPortsForm::SetPortsForm
 * @param inputValue
 * @param outputValue
 * @param parent
 */
SetPortsForm::SetPortsForm(unsigned int *inputValue, unsigned int *outputValue, QWidget *parent) :QWidget(parent), ui(new Ui::SetPortsForm)
{
    ui->setupUi(this);
    this->inputPortsNum = inputValue;
    this->outputPortsNum = outputValue;
    this->setWindowTitle("Input value");

    connect(ui->pushButton, SIGNAL(clicked()),this, SLOT(setUserValue()));
}


/**
 * @brief SetPortsForm::~SetPortsForm
 */
SetPortsForm::~SetPortsForm()
{
    this->inputPortsNum = nullptr;
    this->outputPortsNum = nullptr;
    delete ui;
}


/**
 * @brief SetPortsForm::setUserValue
 */
void SetPortsForm::setUserValue(){
    *this->inputPortsNum = ((unsigned)ui->spinBox->value());
    *this->outputPortsNum = ((unsigned)ui->spinBox_2->value());
    this->close();
}
