/**
  @file AdditionalBlock.h
  @author Utkin Kirill, xutkin00
  @brief Popis tridy AdditionalBlock
  @see AdditionalBlock.cpp
*/

#ifndef ADDITIONBLOCK_H
#define ADDITIONBLOCK_H

#include "block.h"
#include "cmath"

class AdditionBlock: public Block{

public:

    /**
     * @brief AdditionBlock::AdditionBlock primartni konstruktor. Vytvori block a priradi prislusne jmeno. Implicitne nastavi hodnotu
     * Data na hodnotu NaN
     * @param inputPortsNum pocet vstupnich portu
     * @param outputPortsNum pocet vystupnich portu bloku
     */
    AdditionBlock(const int inputPortsNum, const int outputPortsNum);

    /**
     * @brief AdditionBlock::operation Operaci scitani dat a nastavuje vystupni porty na hodnotu data
     */
    void operation() override;
};

#endif // ADDITIONBLOCK_H
