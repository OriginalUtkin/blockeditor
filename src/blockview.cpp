/**
  @file BlockView.cpp
  @author Utkin Kirill, xutkin00
  @brief Defenice tridy BlockView.h
*/

#include "blockview.h"

/**
 * @brief BlockView::BlockView provadi vykresleni bloku po vytvoreni
 * @param parent
 */
BlockView::BlockView(QGraphicsItem *parent) : QGraphicsPathItem(parent){

    QPainterPath painter;
    painter.addRect(-60, -12, 120, 30);
    setPath(painter);

    //Implicitni barva bloku
    setPen(QPen(Qt::black));
    setBrush(Qt::white);

    setFlag(QGraphicsItem::ItemIsMovable); // dovoluje pretahovat item v ramci scene
    setFlag(QGraphicsItem::ItemIsSelectable); // dovoluje vybrat item
    this->setAcceptHoverEvents(true); // dovoluje pouzit hover event

    this->blockWidth = startWidth;
    this->blockHeight = startHeight;
}


/**
 * @brief BlockView::~BlockView destruktor bloku
 */
BlockView::~BlockView(){
    //std::cout << "VIEW BLOCK DESTRUKTOR" << std::endl;
    delete this->blockBackend;
}


/**
 * @brief BlockView::changeColor
 */
void BlockView::changeColor(){

    this->setBrush(Qt::yellow);
    update();
}



/**
 * @brief BlockView::addPort vytvari port uvnitr bloku
 * @param backend logika portu
 * @return ukazatel na port
 */
PortView* BlockView::addPort(Port* backend){

    PortView *port = new PortView(this);
    port->portBackend = backend;
    port->setParentView(this);

    QFontMetrics fm(scene()->font());

    int tmpWidth = fm.width("port");
    int tmpHeight = fm.height();

    if (tmpWidth > blockWidth - startWidth){

        blockWidth = tmpWidth + startWidth;
    }

    blockHeight += tmpHeight;

    QPainterPath painter;
    painter.addRect(-blockWidth/2, -blockHeight/2, blockWidth, blockHeight);
    setPath(painter);

    int changeOnY = -blockHeight / 2 + startHeight + port->getPortWidth();

    foreach(QGraphicsItem *portOnBlock, childItems()) {

        if (portOnBlock->type() != PortView::Type)
            continue;

        PortView *port = (PortView*) portOnBlock;

        if (port->getPortType() == Output){

            port->setPos(blockWidth/2 + port->getPortWidth(), changeOnY);

        }else{

            port->setPos(-blockWidth/2 - port->getPortWidth(), changeOnY);
        }

        changeOnY += tmpHeight;
    }

    return port;
}



/**
 * @brief BlockView::save metoda uklada Block vcetne vsech potr. inormaci do souboru
 * @param ds data stream pro ulozeni
 * @see textRepresentation() na konci souboru
 */
void BlockView::save(std::ofstream& ds){

    ds << this->textRepresentation();
}



/**
 * @brief BlockView::paint prekresli block
 * @param painter ukazatel na objekt pro vykresleni
 * @param option
 * @param widget
 */
void BlockView::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){

    Q_UNUSED(option)
    Q_UNUSED(widget)

    if (isSelected()) {

        painter->setPen(QPen(Qt::darkYellow));
        painter->setBrush(Qt::yellow);

    }else{

        if(!this->blockBackend->getState()){

            painter->setPen(QPen(Qt::black));
            painter->setBrush(Qt::white);

        }else{

            painter->setPen(QPen(Qt::darkRed));
            painter->setBrush(Qt::red);
        }
    }

    painter->drawPath(path());
}


/**
 * @brief BlockView::ports getter metoda ; vrati Seznam vsech portu prislusneho bloku
 * @return
 */
QVector<PortView*> BlockView::getPortsList(){

    QVector<PortView*> portList;

    foreach(QGraphicsItem *sceneItem, childItems()){

        if (sceneItem->type() == PortView::Type)
            portList.append((PortView*) sceneItem);
    }

    return portList;
}


/**
 * @brief BlockView::itemChange
 * @param change
 * @param value
 * @return
 */
QVariant BlockView::itemChange(GraphicsItemChange change, const QVariant &value){

    Q_UNUSED(change);

    return value;
}


/**
 * @brief BlockView::hoverEnterEvent
 */
void BlockView::hoverEnterEvent(QGraphicsSceneHoverEvent*){

    std::cout << this->blockBackend->blockName<<std::endl;
}


/**
 * @brief BlockView::hoverLeaveEvent
 */
void BlockView::hoverLeaveEvent(QGraphicsSceneHoverEvent*){
    //m_hover=false;
}


/**
 * @brief BlockView::type vrati typ objektu v ramci sceny
 * @return type objektu
 * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
 */
int BlockView::type() const{

    return Type;
}


/**
 * @brief BlockView::textRepresentation  vrati textovou reprezentaci bloku
 * @return textovy retezec, reprezentujici blok
 * @todo Vytvorit pretezovani operatoru << pro ulozeni do souboru
 */
std::string  BlockView::textRepresentation(){

    std::string portString = "input:";

    if(this->blockBackend->inpPortsNum > 0){

        for(unsigned int counter = 0 ; counter < this->blockBackend->inpPortsNum ; counter++){
                portString.append("<" + std::to_string(this->blockBackend->getInputPort(counter)->getData()) + ">");
        }
    }


    std::string stringBlock =  "--BLOCK--\n" +
                                this->blockBackend->blockName + "\n" +
                                std::string(this->blockBackend->getState() ? "true" : "false") + "\n" + // stav bloku
                                std::to_string(this->blockBackend->intBlockType()) + "\n" + // typ bloku
                                std::to_string(this->blockBackend->inpPortsNum) + "\n"  +  // pocet vstupnihc portu
                                std::to_string(this->blockBackend->outPortsNum) + "\n"  +  // pocet vystupnich portu
                                portString + "\n" +
                                std::to_string(this->blockBackend->getData()) + "\n"    +  // vnitrni stav loku
                                std::to_string(this->blockBackend->getOperationResult()) + "\n" +    //outputports / operation result
                                std::to_string(this->pos().x()) + "\n" +
                                std::to_string(this->pos().y())+ "\n" +
                                "--END BLOCK--\n";
    return stringBlock;
}


