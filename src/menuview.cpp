/**
  @file MenuView.cpp
  @author Utkin Kirill, xutkin00
  @brief Vytvari interface programu pro sestaveni schemat a vytvari scene.

  @todo Loader (Singleton) for load object from load file
*/

#include "menuview.h"



MenuView::MenuView(QWidget *parent) : QMainWindow(parent){

    this->resize(1200,600); // implicitni nastaveni velikosti okna

    QRect screen = QApplication::desktop()->screenGeometry(); // centrovani hlavniho okna
    this->move((screen.width() - this->width())/2, (screen.height() - this->height())/2);

    this->addBlockFactory = new AdditionalBlockFactory();

    scene = new QGraphicsScene();

    // Vytvoreni polozek menu
    QAction *quitAct = new QAction(tr("&Quit"), this);
    quitAct->setShortcuts(QKeySequence::Quit);
    connect(quitAct, SIGNAL(triggered()), qApp, SLOT(quit()));

    QAction *loadAct = new QAction(tr("&Load"), this);
    loadAct->setShortcuts(QKeySequence::Open);
    connect(loadAct, SIGNAL(triggered()), this, SLOT(loadFile()));

    QAction *saveAct = new QAction(tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    connect(saveAct, SIGNAL(triggered()), this, SLOT(saveFile()));

    QAction *addAct = new QAction(tr("&Summ Block"), this);
    connect(addAct, SIGNAL(triggered()), this, SLOT(addBlock()));

    QAction *addInput = new QAction(tr("&Input Block"), this);
    connect(addInput, SIGNAL(triggered()), this, SLOT(inputBlock()));

    QAction *addOutput = new QAction(tr("&Output Block"), this);
    connect(addOutput, SIGNAL(triggered()), this, SLOT(outputBlock()));

    QAction *addMul = new QAction(tr("&Mul Block"), this);
    connect(addMul, SIGNAL(triggered()), this, SLOT(mulBlock()));

    QAction *addSub = new QAction(tr("&Sub Block"), this);
    connect(addSub, SIGNAL(triggered()), this, SLOT(subBlock()));

    QAction *addInv = new QAction(tr("&Inverter Block"), this);
    connect(addInv, SIGNAL(triggered()), this, SLOT(invBlock()));

    QAction *simulation = new QAction(tr("&Simulation step"), this);
    simulation->setShortcuts(QKeySequence::NextChild);
    connect(simulation, SIGNAL(triggered()), this, SLOT(simulationStart()));

    QAction *simulationRes = new QAction(tr("&Simulation reset"), this);
    connect(simulationRes, SIGNAL(triggered()), this, SLOT(simulationReset()));

    QAction *portSetter = new QAction(tr("&Set ports"), this);
    connect(portSetter, SIGNAL(triggered()), this, SLOT(setNumberPorts()));


    // Vytvoreni menu
    fileMenu = menuBar()->addMenu(tr("&Main"));

    fileMenu->addAction(loadAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(quitAct);

    fileMenu = menuBar()->addMenu(tr("&Simulation"));
    fileMenu->addAction(simulation);
    fileMenu->addAction(simulationRes);

    fileMenu = menuBar()->addMenu(tr("&Elements"));
    fileMenu->addAction(addInput);
    fileMenu->addAction(addInv);
    fileMenu->addAction(addAct);
    fileMenu->addAction(addSub);
    fileMenu->addAction(addMul);
    fileMenu->addAction(addOutput);

    fileMenu = menuBar()->addMenu(tr("&Settings"));
    fileMenu->addAction(portSetter);

    setWindowTitle(tr("Block Scheme Editor"));


   //Vytvoreni vypocetniho modelu
    this->schemeModel = new Model(); // TODO: prevod na Singleton

    // Nastaveni view pro scene
    QDockWidget *dock = new QDockWidget(this);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    view = new QGraphicsView(dock);
    view->setScene(scene);

    view->setRenderHint(QPainter::Antialiasing, true);

    dock->setWidget(view);
    addDockWidget(Qt::LeftDockWidgetArea, dock);

    nodesEditor = new ProgramScene(this);
    nodesEditor->setScene(scene);
}


/**
 * @brief MenuView::~MenuView Destruktor programu
 */
MenuView::~MenuView(){
    delete this->schemeModel; // Smaz model pri uzavreni okna
}


/**
 * @brief MenuView::saveFile provadi ulozeni shematu do zvoleneho souboru
 */
void MenuView::saveFile(){

    QString fname = QFileDialog::getSaveFileName(); // jmeno souboru pro ulozeni

    if (fname.isEmpty())
        return;

    std::string utf8_string = fname.toUtf8().constData(); // QString ma kodovani utf16 -> je nutne prevest na utf8 pro ulozeni do string
    std::ofstream saveFile;

    saveFile.open(utf8_string); // otevreni streamu pro zapis

    nodesEditor->save(saveFile);

    saveFile.close();
}


/**
 * @brief MenuView::loadFile nacita schema ze zvoleneho souboru
 */
void MenuView::loadFile()
{
    QString fname = QFileDialog::getOpenFileName();

    if (fname.isEmpty())
        return;

    std::string utf8_string = fname.toUtf8().constData();
    std::ifstream loadFile;
    loadFile.open(utf8_string);

    nodesEditor->reloadScene();

    this->schemeModel->allBlocks.clear();
    this->schemeModel->activeBlocks.clear();
    this->schemeModel->nextBlocks.clear();

    std::string parse((std::istreambuf_iterator<char>(loadFile)), // nacitani obsahu souboru
                       std::istreambuf_iterator<char>());

    while(!parse.empty()){ // cteni load souboru

        blockData currentData = Parser::getParseData(parse); // Zjisti stav nactene polozky z load souboru

        if(currentData.objectType == "Block"){ // Vytvoreni bloku

            BlockView *newBlock = new BlockView(nullptr); // Vytvoreni View pro nacteny Block

            if(currentData.inputPortNum == 0 && currentData.outputPortNum == 1){ // Load input block

                newBlock->blockBackend = new InputBlock(currentData.inputPortNum, currentData.outputPortNum);

                newBlock->blockBackend->loadBlock(currentData.blockState, InputBlockType, currentData.dataForBlock, currentData.outputData, currentData.inputData);
                this->schemeModel->allBlocks.push_back(newBlock->blockBackend);

                newBlock->setPos(currentData.xScenePosition, currentData.yScenePosition);

                scene->addItem(newBlock);

                for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
                }

            }else if(currentData.inputPortNum == 1 && currentData.outputPortNum == 0){ // Load output block

                newBlock->blockBackend = new OutputBlock(currentData.inputPortNum, currentData.outputPortNum);

                newBlock->blockBackend->loadBlock(currentData.blockState, OutputBlockType, currentData.dataForBlock, currentData.outputData, currentData.inputData);
                this->schemeModel->allBlocks.push_back(newBlock->blockBackend);

                newBlock->setPos(currentData.xScenePosition, currentData.yScenePosition);

                scene->addItem(newBlock);

                for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getInputPort(i));
                }

            }else if(currentData.inputPortNum == 1 && currentData.outputPortNum == 1){ // Load inverter block

                newBlock->blockBackend = new InverterBlock(currentData.inputPortNum, currentData.outputPortNum);

                newBlock->blockBackend->loadBlock(currentData.blockState, OperationBlockType, currentData.dataForBlock, currentData.outputData, currentData.inputData);
                this->schemeModel->allBlocks.push_back(newBlock->blockBackend);

                newBlock->setPos(currentData.xScenePosition, currentData.yScenePosition);

                scene->addItem(newBlock);

                for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getInputPort(i));
                }

                for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
                }

            }else{ // Load other operation blocks

                if(currentData.blockName == "Addblock"){

                    newBlock->blockBackend = addBlockFactory->createBackend(currentData.inputPortNum, currentData.outputPortNum);

                }else if(currentData.blockName == "Subtraction"){

                    newBlock->blockBackend = new SubtractionBlock(currentData.inputPortNum, currentData.outputPortNum);

                }else if(currentData.blockName == "Mulblock"){

                    newBlock->blockBackend = new MultiplicationBlock(currentData.inputPortNum, currentData.outputPortNum);


                }else{ // DIV BLOCK
                    "sdfsdf";
                }

                newBlock->blockBackend->loadBlock(currentData.blockState, OperationBlockType, currentData.dataForBlock, currentData.outputData, currentData.inputData);
                this->schemeModel->allBlocks.push_back(newBlock->blockBackend);

                newBlock->setPos(currentData.xScenePosition, currentData.yScenePosition);

                scene->addItem(newBlock);

                for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getInputPort(i));
                }

                for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
                {
                    newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
                }

            }

        }else{ // Vytvoreni propojeni mezi bloky

            ConnectionView* loadConnection =  new ConnectionView(nullptr);

            scene->addItem(loadConnection);

            // Zajisteni coord. ze load souboru
            QPointF outputPosition =  QPointF(currentData.xScenePosition, currentData.yScenePosition);
            QPointF inputPosition =  QPointF(currentData.xIScenePosition, currentData.yIScenePosition);

            // Vytvoreni propoje mezi bloky
            loadConnection->setOutpuPortPos(outputPosition);
            loadConnection->setInputPortPos(inputPosition);

            // Nastaveni portu do connection
            //std::cout << currentData.xScenePosition << currentData.xIScenePosition
//            PortView* pt = (PortView*)scene->itemAt(outputPosition, QTransform());
//            PortView* rt = (PortView*)scene->itemAt(inputPosition, QTransform());
//            if(pt == nullptr || rt == nullptr ){
//                continue;
//            }
            //std::cout << pt->portBackend->getParent()->blockName << std::endl;
            loadConnection->setOutputPort((PortView*)scene->itemAt(outputPosition, QTransform()));
            loadConnection->setInputPort((PortView*)scene->itemAt(inputPosition, QTransform()));

            // Preved porty do stavu "Connected"
            loadConnection->getOuputPortAdd()->portBackend->setConnected(true);
            loadConnection->getInputPortAdd()->portBackend->setConnected(true);

            loadConnection->updatePath();
        }
    }

    loadFile.close();
}


/**
 * @brief MenuView::addBlock vytvari blok typu Additional (Sum)
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do vypoctoveho modelu tridy
 * Model.
 */
void MenuView::addBlock()
{
    BlockView *newBlock = new BlockView(0);
    newBlock->blockBackend = addBlockFactory->createBackend(this->numberOfInput, this->numberOfOutput);
    this->schemeModel->addBlock(newBlock->blockBackend);

    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getInputPort(i));
    }

    for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
    }

     newBlock->setPos(view->sceneRect().center().toPoint());
}



/**
 * @brief MenuView::inputBlock vytvari blok typu Input
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do schemeModel.allBlocks.
 * @see Model.h
 */
void MenuView::inputBlock()
{
    BlockView *newBlock = new BlockView(0);
    newBlock->blockBackend = new InputBlock(0,1);
    this->schemeModel->addBlock(newBlock->blockBackend);

    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
    }

    newBlock->setPos(view->sceneRect().center().toPoint());
}



/**
 * @brief MenuView::outputBlock vytvari blok typu Output
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do schemeModel.allBlocks.
 * @see Model.h
 */
void MenuView::outputBlock()
{
    BlockView *newBlock = new BlockView(0);
    newBlock->blockBackend = new OutputBlock(1,0);
    this->schemeModel->addBlock(newBlock->blockBackend);

    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getInputPort(i));
    }

     newBlock->setPos(view->sceneRect().center().toPoint());
}


/**
 * @brief MenuView::mulBlockvytvari blok typu Mul
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do schemeModel.allBlocks.
 * @see Model.h
 */
void MenuView::mulBlock()
{
    BlockView *newBlock = new BlockView(0);
    newBlock->blockBackend = new MultiplicationBlock(this->numberOfInput, this->numberOfOutput);
    this->schemeModel->addBlock(newBlock->blockBackend);

    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getInputPort(i));
    }

    for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
    }

    newBlock->setPos(view->sceneRect().center().toPoint());
}


/**
 * @brief MenuView::subBlock vytvari blok typu Subtraction
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do schemeModel.allBlocks.
 * @see Model.h
 */
void MenuView::subBlock()
{
    BlockView *newBlock = new BlockView(0);
    newBlock->blockBackend = new SubtractionBlock(this->numberOfInput,this->numberOfOutput);
    this->schemeModel->addBlock(newBlock->blockBackend);
    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getInputPort(i));
    }

    for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
    }

    newBlock->setPos(view->sceneRect().center().toPoint());
}


/**
 * @brief MenuView::invBlock vytvari blok typu Inverter
 * ,prislusny backend a vstupni / vystupni porty na zaklade backendu.
 * Backend kazdeho bloku pak pridan do schemeModel.allBlocks.
 * @see Model.h
 */
void MenuView::invBlock(){

    BlockView *newBlock = new BlockView(nullptr);
    newBlock->blockBackend = new InverterBlock(1,1);
    this->schemeModel->addBlock(newBlock->blockBackend);

    scene->addItem(newBlock);

    for (unsigned int i = 0; i < newBlock->blockBackend->inpPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getInputPort(i));
    }

    for (unsigned int i = 0; i < newBlock->blockBackend->outPortsNum; i++)
    {
        newBlock->addPort(newBlock->blockBackend->getOutputPort(i));
    }

    newBlock->setPos(view->sceneRect().center().toPoint());
}


/**
 * @brief MenuView::simulationStart
 */
void MenuView::simulationStart(){

    // Симуляция : TODO : Предварительная подготовка перед симуляцией
    //zjiskej vsichni backendy ze View
    foreach(QGraphicsItem *item, scene->items())
    {
        if (item->type() == BlockView::Type)
        {
            for(int i =0 ; i < ((BlockView*)item)->getPortsList().size(); i++){

                if(((BlockView*)item)->getPortsList().at(i)->connList.size() > 0){ // zajisteni vsech propojeni mezi bloky do vypoctoveho modelu

                    if(this->schemeModel->all_connections.indexOf(((BlockView*)item)->getPortsList().at(i)->connList.at(0)) == -1){ // propojeni jiz existuje v modelu
                        this->schemeModel->all_connections.push_back(((BlockView*)item)->getPortsList().at(i)->connList.at(0));
                    }
                }
            }
        }

    }


    if(this->schemeModel->simulationCheck()){

        if(schemeModel->getSimulationState()){

            schemeModel->simulationStep();
            scene->update(scene->sceneRect());

        }
    }else{

        this->errorForms.push_back( new ErrorForm(nullptr) );
        this->errorForms.at(this->errorForms.size()-1)->show();
    }



    std::cout << "CONNECTION START" <<std::endl;
    for(int i = 0 ; i < this->schemeModel->all_connections.size(); i++){
        std::cout << this->schemeModel->all_connections.at(i) <<std::endl;

    }
    std::cout << "CONNECTION END" <<std::endl;
}


/**
 * @brief MenuView::setNumberPorts metoda zjiskava data ze SetPortsForm a vytvari bloky na zaklade ziskane informace
 * @see SetPortsForm.cpp
 */
void MenuView::setNumberPorts(){

    this->formSetter.push_back( new SetPortsForm(&this->numberOfInput, &this->numberOfOutput, 0));
    this->formSetter.at(this->formSetter.size()-1)->setAttribute(Qt::WA_DeleteOnClose); // <<- ?? Can this solution  call SIGPROCESS ??
    this->formSetter.at(this->formSetter.size()-1)->show();
}


/**
 * @brief MenuView::simulationReset Obnovi simulaci
 */
void MenuView::simulationReset(){

    this->schemeModel->allBlocks.clear();
    this->schemeModel->allConnections.clear();

    foreach(QGraphicsItem *item, scene->items()){

        if (item->type() == BlockView::Type){

            if(((BlockView*)item)->blockBackend->getBlockType() == InputBlockType){

                if(((BlockView*)item)->blockBackend->getState() == false){

                    ((BlockView*)item)->blockBackend->changeState();
                }

            }else{

                ((BlockView*)item)->blockBackend->clearInputPorts();

                if(((BlockView*)item)->blockBackend->getState() != false){

                    ((BlockView*)item)->blockBackend->changeState();
                }
            }

            ((BlockView*)item)->blockBackend->setData(std::numeric_limits<double>::quiet_NaN());
            ((BlockView*)item)->blockBackend->setOutputPorts(std::numeric_limits<double>::quiet_NaN());
            this->schemeModel->allBlocks.push_back(((BlockView*)item)->blockBackend);

        }else{
            continue;
        }
    }


    this->schemeModel->resetSimulationState();

    scene->update(scene->sceneRect());
}

