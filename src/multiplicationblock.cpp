/**
  @file MultiplicationBlock.cpp
  @author Utkin Kirill (xutkin00)
  @brief Definice tridy MultiplicationBlock.h
  @see Block.h
*/


#include "multiplicationblock.h"

/**
 * @brief MultiplicationBlock::MultiplicationBlock Primarni konstruktor tridy
 * @param inputPortsNum pocet vstupnich portu
 * @param outputPortsNum pocet vystupnich portu
 */
MultiplicationBlock::MultiplicationBlock(const int inputPortsNum, const int outputPortsNum) : Block(inputPortsNum,outputPortsNum){

    this->blockName = "Mulblock"; // chyba
    blockType = OperationBlockType;

    this->data = std::numeric_limits<double>::quiet_NaN();
    this->operationResult = std::numeric_limits<double>::quiet_NaN();
}


/**
 * @brief MultiplicationBlock::operation overreide abstraktni metody operation ze abstaktni tridy block. Vynasobi cisla ze vstupnich portu.
 */
void MultiplicationBlock::operation() {

    if(!checkInputPorts()){
        this->data = std::numeric_limits<double>::quiet_NaN();

    }else{
            this->data = 1;


            for(unsigned int i = 0 ; i < inpPortsNum; ++i){
                    this->data *= inputPorts[i].getData();
            }

           this->operationResult = this->data;
    }


    setOutputPorts(this->operationResult);
}

