/**
  @file ConnectionView.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro ConnectionView
  @see ConnectionView.cpp
*/

#ifndef CONNECTIONVIEW_H
#define CONNECTIONVIEW_H

#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include <QBrush>
#include <QPen>
#include <string>
#include <fstream>
#include <iostream>

#include "connection.h"
#include "portview.h"

class Connection;
class PortView;

class ConnectionView : public QGraphicsPathItem
{
public:

    // Definice typu objektu uvnitr scene (http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var)
    enum { Type = QGraphicsItem::UserType + 2 };


    /**
     * @brief ConnectionView::ConnectionView Primarni konstruktor tridy, ktery vytvari vzhled propojeni
     * @param parent
     */
    ConnectionView(QGraphicsItem *parent = nullptr);


    /**
     * @brief ConnectionView::~ConnectionView destruktor tridy. nastavuje pole input / output portu na false a odstrani zaznam z
     * propoje
     */
    ~ConnectionView();


    /**
     * @brief ConnectionView::type Getter metoda ; vrati typ objektu uvnitr sceny
     * @return type objektu
     * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
     */
    int type() const;


    /**
     * @brief ConnectionView::setOutpuPortPos setter metoda ; nastavi pozici vystupniho portu.
     * @param tmpPosition hodnota typu QPointF, obsahujici pozici vystupniho portu
     */
    void setOutpuPortPos(const QPointF &tmpPosition);


    /**
     * @brief ConnectionView::setOutputPort setter metoda ; ulozi view vystupniho portu do pole outputPortAdd pristup do
     * logiky portu pri provadeni operaci.
     * @param portAddress adresa view vystupniho portu
     */
    void setOutputPort(PortView* portAddress);


    /**
     * @brief ConnectionView::getOuputPortAdd getter metoda ; vrati adresu output portu
     * @return adresa output portu
     */
    PortView* getOuputPortAdd() const;


    /**
     * @brief ConnectionView::setInputPortPos setter metoda ; nastavi pozici vstupniho portu.
     * @param tmpPosition hodnota typu QPointF, obsahujici pozici vstupniho portu
     */
    void setInputPortPos(const QPointF& tmpPosition);



    /**
     * @brief ConnectionView::setInputPort setter metoda ; ulozi view vstupniho portu do pole outputPortAdd pro pristup do
     * logiky portu pri provadeni operaci.
     * @param portAddress adresa view vystupniho portu
     */
    void setInputPort(PortView* portAddress);


    /**
     * @brief ConnectionView::getInputAdd getter metoda ; vrati adresu input portu
     * @return adresa input portu
     */
    PortView* getInputPortAdd() const;


    /**
     * @brief ConnectionView::updatePosFromPorts metoda se stara o zajisteni coord. uvnitr scene pri zmene pozici bloku
     */
    void updatePosFromPorts();


    /**
     * @brief ConnectionView::updatePath metoda prekresli propojeni mezi bloky
     * @see PortView.cpp
     */
    void updatePath();


    /**
     * @brief ConnectionView::pushData metoda preposle data ze vystupnich portu na vstupni port dalsiho bloku pres propoj connection
     */
    void pushData();


    /**
     * @brief ConnectionView::save uklada textovou reprezentaci propojeni do souboru
     * @param ds stream pro ulozeni text. rept. propoje
     */
    void save(std::ofstream& ds );

    PortView *outputPortAdd = nullptr; // pointer na  vystupni port
    PortView *inputPortAdd = nullptr; // pointer na  vstupni port

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent*);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent*);

private:

    /**
     * @brief ConnectionView::textRepresentation vrati textovou reprezentaci propoje
     * @return retezec, reprezentujici propoj
     * @todo Vytvorit pretezovani operatoru << pro ulozeni do souboru
     */
    std::string textRepresentation();

    QGraphicsTextItem *connectionData = nullptr; //stav propojeni
    QPointF outputPortPos; // pozice vystupniho bloku
    QPointF inputPortPos; //pozcice vstupniho bloku

};

#endif // CONNECTIONVIEW_H
