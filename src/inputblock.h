/**
  @file InputBlock.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro Block.cpp
  @see InputBlock.cpp
*/

#ifndef INPUTBLOCK_H
#define INPUTBLOCK_H

#include "block.h"
#include <limits>


class InputBlock : public Block
{
public:

    /**
     * @brief InputBlock::InputBlock konstruktor tridy InputBlock. Implicitne uklada do pole inputData hodnotu NaN
     * @param inputPortsNum pocet vsupnich portu bloku (musi byt nula pro vstupni port)
     * @param outputPortsNum pocet vystupnich portu bloku
     */
    InputBlock(const int inputPortsNum, const int outputPortsNum);


    /**
     * @brief InputBlock::operation nastavuje vystupni porty inputBloku
     */
    void operation() override ;
};

#endif // INPUTBLOCK_H
