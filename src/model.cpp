/**
  @file model.cpp
  @author Utkin Kirill (xutkin00)
  @brief Definice tridy Model
*/


#include "model.h"

/**
 * @brief Model::Model prazany primarni konstruktor pro vytvoreni vypocetniho modelu
 */
Model::Model(){

}


/**
 * @brief Model::simulationCheck metoda pro kontrolu spravnosti sestaveni schemata
 * @return true, pokud lze provest vypocet, jinak vrati false
 */
bool Model::simulationCheck() const{

    for(unsigned int counter = 0 ; counter < this->allBlocks.size(); counter++){ // Kontrola, zda-li vsichni porty jsou napojene na jiny port

        if(this->allBlocks[counter]->inpPortsNum > 0){ // Blok ma alespon jeden vstupni port

            for(unsigned int portCounter = 0 ; portCounter < this->allBlocks[counter]->inpPortsNum; portCounter++)

                if(!allBlocks[counter]->inputPorts[portCounter].isConnected()){

                    return false;
                }

        }else{ // Blok ma 0 vstupnich portu
            continue;
        }
    }


    for(unsigned int counter = 0 ; counter < this->allBlocks.size(); counter++){ // Kontrola, zda-li vsichni input bloky maji pocatecni hodnotu

        if((std::isnan(allBlocks[counter]->getData())) && (allBlocks[counter]->getBlockType() == InputBlockType)){
            return false;
        }
    }

    return true;
}

/**
 * @brief Model::getSimulationState getter, ktery vrati stav vypocetniho modelu
 * @return true, pokud je mozne provadet vypocet dal, jinak false , ktery signalizuje ze vypocet ukoncen
 */
bool Model::getSimulationState() const{

    return this->simulationState;
}


/**
 * @brief Model::resetSimulationState nastavi hodnotu simulationState na true
 */
void Model::resetSimulationState(){

    this->simulationState = true;
}


/**
 * @brief Model::simulationStep jeden krok vypocetniho modelu ktery provadi operaci ve vsech aktivnich blocich
 * @return true, pokud existuje dalsi krok vypocetniho modelu, jinak vraci false
 */
bool Model::simulationStep(){


    std::cout << allBlocks.size() << std::endl;
    setSimulationStepData();

    if(nextBlocks.size() == 0){
        this->simulationState = false;
        this->closedConnection.clear();
    }

    blockOperation();

    pushDataToBlock();

    changeToPassive();

    for(unsigned int counter = 0 ; counter < this->allBlocks.size(); counter++){ // Kontrola, zda-li vsichni porty jsou napojene na jiny port

        if(this->allBlocks[counter]->inpPortsNum > 0){ // Blok ma alespon jeden vstupni port
            //allBlocks[counter]->blockName;
            for(unsigned int portCounter = 0 ; portCounter < this->allBlocks[counter]->inpPortsNum; portCounter++)

                std::cout << allBlocks[counter]->inputPorts[portCounter].getData() << std::endl;
        }
    }


    eraseBlockFromNext();

    activateBlocks();

    clearSimulationData();

    return this->simulationState;
}


/**
 * @brief Model::setSimulationStepData nastavuje data pro vypocetny krok. Nastavuje bloky pro dalsi iteraci
 */
void Model::setSimulationStepData(){

    for(int i = 0; i < all_connections.size(); i++){

        if(all_connections[i]->outputPortAdd->portBackend->getParent()->getState() == true){ // Zajisteni aktivnich bloku v systemu

            if (!std::binary_search(activeBlocks.begin(), activeBlocks.end(), all_connections[i]->outputPortAdd->portBackend->getParent())){ //Zakisteni active bloku
                activeBlocks.push_back(all_connections[i]->outputPortAdd->getParent()->blockBackend);
            }

            if (!std::binary_search(nextBlocks.begin(), nextBlocks.end(), all_connections[i]->inputPortAdd->portBackend->getParent())){ // Zajisteni bloku pro dalsi iteraci
                    nextBlocks.push_back(all_connections[i]->inputPortAdd->portBackend->getParent());
            }

        }else{ // Zustal pouze vystupni block

            if((all_connections[i]->inputPortAdd->portBackend->getParent()->getState() == true) &&
               (all_connections[i]->inputPortAdd->portBackend->getParent()->getBlockType() == OutputBlockType)){ // pokud block jeste neexistuje v seznamu

                if (!std::binary_search(nextBlocks.begin(), nextBlocks.end(), all_connections[i]->inputPortAdd->portBackend->getParent())){
                    activeBlocks.push_back(all_connections[i]->inputPortAdd->portBackend->getParent());
                }
            }
        }
    }
}


/**
 * @brief Model::blockOperation provadi operaci aktivniho bloku
 * @see Block.Oeration
 */
void Model::blockOperation(){

    for(unsigned int counter = 0 ; counter < activeBlocks.size(); counter++){

        for(unsigned int counter2 = 0 ; counter2 < allBlocks.size(); counter2++){

            if(activeBlocks[counter] == allBlocks[counter2])
                allBlocks[counter2]->operation();
        }
    }
}


/**
 * @brief Model::pushDataToBlock provadi prevod dat z aktivniho bloku do input portu dalsiho(seznam nextBlocks) bloku
 * @see Connection.pushData()
 */
void Model::pushDataToBlock(){

    for(int i = 0; i < all_connections.size(); i++){

        if(all_connections[i]->outputPortAdd->portBackend->getParent()->getState() == true){

            all_connections[i]->pushData();
            closedConnection.push_back(all_connections[i]);
        }
    }
}


/**
 * @brief Model::changeToPassive provadi blokovani bloku po porvedeni operaci
 * @see block.operation()
 */
void Model::changeToPassive(){

    for(unsigned int counter = 0 ; counter < activeBlocks.size(); counter++){

        for(unsigned int counter2 = 0 ; counter2 < allBlocks.size(); counter2++){

            if(activeBlocks[counter] == allBlocks[counter2])
                allBlocks[counter2]->changeState();
        }
    }
}


/**
 * @brief Model::eraseBlockFromNext vyhodi blok ze seznamu bloku pro dalsi vypocetni iteraci
 */
void Model::eraseBlockFromNext(){

    for(unsigned int counter = 0 ; counter < nextBlocks.size(); counter++){

        for(unsigned int portCounter = 0 ; portCounter < nextBlocks[counter]->inpPortsNum; portCounter++){

            if((std::isnan(nextBlocks[counter]->inputPorts[portCounter].getData())) || (nextBlocks[counter]->getBlockType() == InputBlockType)){
                nextBlocks.erase(nextBlocks.begin() + counter);
            }
        }
    }
}


/**
 * @brief Model::activateBlocks nastavuje stav bloku na Active pro vypocet v dalsim kroku vypocetniho modelu
 */
void Model::activateBlocks(){

    for(unsigned long counter =0 ; counter < nextBlocks.size(); counter++){

        for(unsigned int counter2 = 0 ; counter2 < allBlocks.size(); counter2++){

            if(nextBlocks[counter] == allBlocks[counter2])
                allBlocks[counter2]->changeState();
        }
    }
}


/**
 * @brief Model::addBlock pridava blok do vypocetniho modelu. Funkce se zavola po vytvoreni bloku
 * @param value adresa backendu bloku, ktery je nutne pridat do vypocetniho systemu
 */
void Model::addBlock(Block *value){

    this->allBlocks.push_back(value);
}


/**
 * @brief Model::clearSimulationData provadi operaci vyprazdneni vsech elementu ze seznamu activeBlocks a nextBlocks
 */
void Model::clearSimulationData(){

    activeBlocks.clear();
    nextBlocks.clear();
}



