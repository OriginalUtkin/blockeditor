/**
  @file Parser.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro Parser.cpp
  @todo use singleton pattern
*/

#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cassert>
#include <cmath>

#include "blockdata.h"

// TODO : USE SINGLETON PATTERN
class Parser
{
public:

    /**
     * @brief Parser::Parser Implicitni kostruktor tridy
     */
    Parser();


    /**
     * @brief Parser::getParseData zjiskava polozku z load souboru pro parsovani. Pouziva dalsi pomocne funkci
     * @param fileLines obsah load souboru
     * @return struct hodnotu , potrebnou pro vytvoreni objektu ci propojeni
     * @see blockData.h
     */
    static blockData getParseData(std::string &fileLines);

private:

    /**
     * @brief Parser::parser vrati polozku ve formatu vector<string>
     * @param fileLines obsah load souboru
     * @return jednu polozku ve formatu vector<string>
     */
    static std::vector<std::string> parser(std::string& fileLines);


    /**
     * @brief Parser::convertData konvertuje data z vector<string> na blockData
     * @param data polozka z load souboru veformatu vector<string>
     * @return polozku BlockData pro vytvoreni objektu ci propojeni
     * @see BlockData.h
     */
    static blockData convertData(const std::vector<std::string>& data);


    /**
     * @brief Parser::inputValueParse Provadi parsovani retezce ze load souboru obsahujiciho vstu[ni hodnoty a uklada do vector
     * @param inputValueString textovy retezec ve tvaru input:<value
     * @return vector<double> obsahujici data ze vsech portu (prvni hodnota = horni port...)
     */
    static std::vector<double> inputValueParse(const std::string& inputValueString);


    /**
     * @brief Parser::to_bool prevadi textovou reprezentaci do bool
     * @param str retezec pro prevod
     * @return vrati true v pripade ze hodnota ze str je true, jinak vrati false
     */
    static bool to_bool(std::string str);
};

#endif // PARSER_H
