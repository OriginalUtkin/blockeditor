/**
  @file PortView.cpp
  @author Utkin Kirill (xutkin00)
  @brief defenice tridy PortView
*/

#include "portview.h"


/**
 * @brief PortView::PortView Primarni konstruktor tridy
 * @param parent
 */
PortView::PortView(QGraphicsItem *parent) : QGraphicsPathItem(parent){

    recWidth = 3;
    portMargin = 3;
    QPainterPath p;

    p.addRect(-recWidth + 1.5, -recWidth + 1.4, 3.2 * recWidth, 3.2 * recWidth);
    setPath(p);
    setPen(QPen(Qt::black));
    setBrush(Qt::black);
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
}


/**
 * @brief PortView::~PortView destruktor tridy
 */
PortView::~PortView(){

    foreach(ConnectionView *conn, connList)
        delete conn;
}


/**
 * @brief PortView::setParentView metoda nastavuje odkaz na blok, ke kteremu patri tento port
 * @param parentAdd ukazatel na rodic. blok
 */
void PortView::setParentView(BlockView* parentAdd){

    this->parentBlockView = parentAdd;
}



/**
 * @brief PortView::getPortWidth getter metoda ; vrati vysku (=sirce) portu
 * @return hodnotu int , reprezentujici velikost portu
 * @see Block.cpp
 */
int PortView::getPortWidth() const{

    return this->recWidth;
}

//TODO
bool PortView::getPortType() const{

    return this->portBackend->getPortType();
}


//TODO
QVector<ConnectionView*>& PortView::getConnectionList(){

    return connList;
}


/**
 * @brief PortView::getParent getter metoda; vrati odkaz na blok, ke kteremu patri port
 * @return ukazatel na BlockView
 */
BlockView* PortView::getParent() const{
    return parentBlockView;
}

bool PortView::isConnected(PortView *other){

    foreach(ConnectionView *conn, connList){

        if (conn->getOuputPortAdd() == other || conn->getInputPortAdd() == other)
            return true;
    }

    return false;
}


/**
 * @brief PortView::itemChange meni propojeni mezi bloky pri zmene pozice bloku
 * @param change typ zmeny
 * @param value
 * @return QGraphicsItem::itemChange
 * @see http://doc.qt.io/qt-5/qgraphicsitem.html#itemChange
 */
QVariant PortView::itemChange(GraphicsItemChange change, const QVariant &value){
    if (change == ItemScenePositionHasChanged)
    {
        foreach(ConnectionView *conn, connList)
        {
            conn->updatePosFromPorts();
            conn->updatePath();
        }
    }
    return value;
}


/**
 * @brief PortView::type vrati typ objektu v ramci sceny
 * @return type objektu
 * @see http://doc.qt.io/qt-5/qgraphicsitem.html#UserType-var
 */
int PortView::type() const{

    return Type;
}
