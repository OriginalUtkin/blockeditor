#-------------------------------------------------
#
# Project created by QtCreator 2018-05-06T20:39:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BlockEditor
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    additionalblockfactory.cpp \
    additionalblock.cpp \
    block.cpp \
    blockfactory.cpp \
    blockview.cpp \
    connection.cpp \
    connectionview.cpp \
    errorform.cpp \
    inputblock.cpp \
    inputform.cpp \
    inverterblock.cpp \
    menuview.cpp \
    model.cpp \
    multiplicationblock.cpp \
    outputblock.cpp \
    outputform.cpp \
    parser.cpp \
    port.cpp \
    portview.cpp \
    setportsform.cpp \
    subtractionblock.cpp \
    main.cpp \
    programscene.cpp

HEADERS += \
    additionalblockfactory.h \
    additionalblock.h \
    block.h \
    blockdata.h \
    blockfactory.h \
    blocktype.h \
    blockview.h \
    connection.h \
    connectionview.h \
    datatype.h \
    errorform.h \
    inputblock.h \
    inputform.h \
    inverterblock.h \
    menuview.h \
    model.h \
    multiplicationblock.h \
    outputblock.h \
    outputform.h \
    parser.h \
    port.h \
    portconnection.h \
    porttype.h \
    portview.h \
    setportsform.h \
    subtractionblock.h \
    programscene.h

FORMS += \
    inputform.ui \
    setportsform.ui \
    outputform.ui \
    errorform.ui

