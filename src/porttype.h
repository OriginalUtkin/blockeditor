/**
  @file PortType.h
  @author Utkin Kirill, xutkin00
  @brief defenuje typy portu
*/

#ifndef PORTTYPE_H
#define PORTTYPE_H

typedef enum{

    Input, // defenuje vstupni port
    Output // defenuje vystupni port

}PortType;

#endif // PORTTYPE_H


