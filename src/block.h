/**
  @file Block.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro Block.cpp
  @see Block.cpp
*/


#ifndef BLOCK_H
#define BLOCK_H

#include "port.h"
#include "blocktype.h"
#include <string>
#include <cmath>
#include <vector>


static int blockNumbers = 0;
class Block
{

public:

    /**
     * @brief Block::Block Primarni konstruktor tridy Block
     * @param inputPortsNum pocet vstupnich portu bloku
     * @param outputPortsNum pocet vystupnich portu bloku
     */
    Block(const int inputPortsNum, const int outputPortsNum);


    /**
     * @brief Block::changeState setter metoda ; nastavi pole state na opacnou hodnotu
     */
    virtual ~Block();



    /**
     * @brief Block::getState Vraci stav bloku
     * @return true pokud blok je aktivni a lze provest vypocet, jinak vraci false
     */
    bool getState() const;


    /**
     * @brief Block::getBlockType getter metoda ; vrati typ bloku
     * @return hodnota enum definujici druh bloku
     * @see BlockType.h
     */
    BlockType getBlockType() const;


    /**
     * @brief Block::getData funkce vrati hodnotu, ktera je ulozena na ve vstupnich portech bloku
     * @return hodnota typu double
     */
    double getData() const;


    /**
     * @brief Block::getOperationResult getter metoda ; vrati vysledek, ulozeny na vystupnich portech po provadeni vypocetne operaci
     * @return double hodnota - vysledek operaci bloku
     */
    double getOperationResult() const;


    /**
     * @brief Block::setData nastavuje hodnotu na vstupnich portech bloku
     * @param value hodnota, kterou je nutne ulozit do vstupnich portu
     */
    void setData(double value);

    /**
     * @brief Block::getInputPort getter metoda ; metoda vrati adresu vstupniho portu
     * @param i ciiselny identifikator portu
     * @return adresa vstupniho portu
     */
    Port* getInputPort(unsigned int i);


    /**
     * @brief clearInputPorts metoda nastavi vsichni vstupni porty na hodnotu NaN
     */
    void clearInputPorts();


    /**
     * @brief Block::getOutputPort getter metoda ; vrati adresu vystupniho portu
     * @param i ciiselny identifikator portu
     * @return adresa vystupniho portu
     */
    Port* getOutputPort(unsigned int i);


    /**
     * @brief Block::setOutputPorts nastavuje hodnoty vystupnich portu bloku
     * @param value hodnota, kterou nutne ulozit do vystupnich portu bloku
     */
    void setOutputPorts(const double value);


    /**
     * @brief Block::intBlockType metoda prevadi enum hodnotu blockType na ciselnou hodnotu typu int pro ulozeni do load souboru
     * @return int hodnota reprezentujici typ bloku
     * @see BlockType
     */
    int intBlockType() const;


    /**
     * @brief Block::setOpResult
     * @param value
     * @return
     */
    void setOpResult(double value);

    /**
     * @brief Block::changeState setter metoda ; nastavi pole state na opacnou hodnotu
     */
    void changeState();

    Port *inputPorts = nullptr; // pole vstupnich portu bloku
    Port *outputPorts = nullptr; // pole vystupnich portu bloku

    /**
     * @brief operation abstraktni metoda, kazdy dalsi blok musi tuto operaci realizovat
     */
    virtual void operation() = 0 ;

    /**
     * @brief Block::loadBlock metoda nastavuje pole instanci na hodnoty ze load souboru
     * @param blockState stav bloku po ulozeni
     * @param type typ bloku
     * @param inputData data na vstupnich portech
     * @param outputData data na vystupnich portech
     */
    void loadBlock(const bool blockState, const BlockType type, const double blockOperationData, const double outputData, std::vector<double> inputPortsData);

    std::string blockName = ""; // jmeno bloku
    const unsigned int inpPortsNum; // pocet vstupnich portu bloku
    const unsigned int outPortsNum; // pocet vystupnich portu bloku

protected:

    /**
     * @brief Block::checkPorts metoda provadi kontrolu, zda-li vsichni porty maji hodnotu odlisnou od NaN
     * @return true, pokud vsichni porty maji nastavenou hodnotu, jinak vrati false
     */
    bool checkInputPorts() const;


    double data; // data, definujici vnitrni stav bloku -> predelat na union pro ulozeni ruznych typu dat
    BlockType blockType; //druh bloku
    bool state; //stav bloku
    double operationResult; // vysledek pro predani do vystupnich portu
};

#endif // BLOCK_H
