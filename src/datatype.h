/**
  @file DataType.h
  @author Utkin Kirill, xutkin00
  @brief Defenuje typ dat uvnitr bloku (Types of operation for blocks)
*/

#ifndef DATATYPE_H
#define DATATYPE_H

typedef enum{
    Arithmetic, // Aritmeticka data
    Logic // Logicka data
}DataType ;

#endif // DATATYPE_H

