/**
  @file SubtractionBlock.h
  @author Utkin Kirill, xutkin00
  @brief hlavickovy soubor pro SubtractionBlock.cpp
  @see SubtractionBlock.cpp
*/

#ifndef SUBTRACTIONBLOCK_H
#define SUBTRACTIONBLOCK_H

#include "block.h"
#include "cmath"

class SubtractionBlock : public Block
{
public:

    /**
     * @brief SubtractionBlock::SubtractionBlock Primarni konstruktor tridy
     * @param inputPortsNum pocet vstupnich portu
     * @param outputPortsNum pocet vystupnich portu
     */
    SubtractionBlock(const int inputPortsNum, const int outputPortsNum);



    /**
     * @brief SubtractionBlock::operation provadi vynasobeni dat ze vstupnich portu bloku a uklada vysledek do vystupnich portu
     */
    void operation() override;
};

#endif // SUBTRACTIONBLOCK_H
