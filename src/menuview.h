/**
  @file MenuView.h
  @author Utkin Kirill, xutkin00
  @brief Hlavickovy soubor pro MenuView.cpp
  @see MenuView.cpp
*/

#ifndef MENUVIEW_H
#define MENUVIEW_H

#include <QMainWindow>
#include <QtWidgets>
#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>
#include <QGraphicsScene>
#include <QFileDialog>
#include <algorithm>
#include <cmath>

#include "model.h"
#include "setportsform.h"
#include "blockview.h"
#include "programscene.h"
#include "additionalblockfactory.h"

#include "portview.h"
#include "additionalblock.h"
#include "multiplicationblock.h"
#include "inverterblock.h"
#include "subtractionblock.h"
#include "inputform.h"
#include "errorform.h"

class ProgramScene;
class Model;

class MenuView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MenuView(QWidget *parent = nullptr);
    ~MenuView();

private slots:
    void saveFile();
    void loadFile();
    void addBlock();
    void inputBlock();
    void outputBlock();
    void simulationStart();
    void simulationReset();
    void mulBlock();
    void subBlock();
    void invBlock();
    void setNumberPorts();

private:
    unsigned int numberOfInput = 2;
    unsigned int numberOfOutput = 1;
    ProgramScene *nodesEditor = nullptr;
    QMenu *fileMenu = nullptr;
    QGraphicsView *view = nullptr;
    QGraphicsScene *scene = nullptr;
    QVector<SetPortsForm*> formSetter;
    QVector<ErrorForm*> errorForms;
    AdditionalBlockFactory* addBlockFactory = nullptr;

    Model* schemeModel = nullptr;
};

#endif // MENUVIEW_H
